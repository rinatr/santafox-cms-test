<?php

$type_langauge = 'en';

$il['news_base_name'] = 'News';
$il['newsi_modul_base_name1'] = 'Main news';

$il['news_property_img_source_width'] = 'The width of the original image';
$il['news_property_img_source_height'] = 'The height of the original image';
$il['news_property_img_big_width'] = 'The width of the largest image in pixels';
$il['news_property_img_big_height'] = 'The height of the big picture in pixels';
$il['news_property_img_small_width'] = 'The width of the small picture in pixels';
$il['news_property_img_small_height'] = 'The height of the small picture in pixels';
$il['news_property_deliver'] = 'The value of attribute "The newsletter"';
$il['news_property_lenta'] = 'The value of attribute "in the film"';
$il['news_property_rss'] = 'The value of attribute "The RSS"';
$il['news_property_news_per_page'] = 'News page for AI';
$il['news_property_page_for_lenta'] = 'Page where the archive is formed (to be completed in the case to the news module address other newswires)';
$il['news_property_copyright_transparency'] = 'Transparent watermark';

$il['news_pub_show_lenta'] = 'Form a band';
$il['news_pub_show_lenta_template'] = 'Template Tape';
$il['news_pub_show_lenta_limit'] = 'Number of news in the tape';
$il['news_pub_show_lenta_type'] = 'Type selection';
$il['news_pub_show_lenta_type_default'] = 'Default';
$il['news_pub_show_lenta_type_past'] = 'From current date to the past';
$il['news_pub_show_lenta_type_future'] = 'From the current date into the future';
$il['news_pub_show_lenta_page'] = 'Page where the archive is formed';
$il['news_pub_show_lenta_id_modules'] = 'Module ID (comma), whose news as well get into the tape';

$il['news_property_watermark'] = 'Whether to put a watermark?';
$il['news_property_path_to_copyright_file'] = 'Select a watermark';
$il['news_property_copyright_position'] = 'Location watermark';
$il['news_property_copyright_position_0'] = 'In the center';
$il['news_property_copyright_position_1'] = 'Top-Left';
$il['news_property_copyright_position_2'] = 'Top-Right';
$il['news_property_copyright_position_3'] = 'Bottom-Right';
$il['news_property_copyright_position_4'] = 'Bottom-Left';

$il['news_pub_show_archive'] = 'Generate archive';
$il['news_pub_show_archive_template'] = 'Template';
$il['news_pub_show_archive_limit'] = 'Max on the news page';
$il['news_pub_show_archive_type'] = 'Type selection';
$il['news_pub_show_archive_default'] = 'Regardless of the choice';
$il['news_pub_show_archive_past'] = 'From current date to the past';
$il['news_pub_show_archive_future'] = 'From the current date into the future';
$il['news_pub_show_lenta_type_random'] = 'Random record';

$il['news_pub_show_sorting'] = 'Show sorting';
$il['news_pub_show_selection'] = 'Show selection';
$il['news_pub_show_selection_template'] = 'Template';

$il['news_menu_label'] = 'Management';
$il['news_menu_show_list'] = 'View';
$il['news_menu_between'] = 'Over a period';
$il['news_menu_add_new'] = 'Add';

$il['news_show_list_action_lenta_on'] = 'Show in tape';
$il['news_show_list_action_lenta_off'] = 'Do not display in the Ribbon';
$il['news_show_list_action_available_on'] = 'Make visible';
$il['news_show_list_action_available_off'] = 'Make no visible';
$il['news_show_list_action_rss_on'] = 'Show in RSS';
$il['news_show_list_action_rss_off'] = 'Do not display in RSS';
$il['news_show_list_action_delete'] = 'Remove';
$il['news_show_list_action_move'] = 'Move';

// Заголовки в show_list
$il['news_item_id'] = 'ID';
$il['news_item_date'] = 'Date';
$il['news_item_header'] = 'Title';
$il['news_item_available'] = 'Visibility';
$il['news_item_lenta'] = 'The film';
$il['news_item_rss'] = 'The RSS';
$il['news_item_author'] = 'Author';
$il['news_item_actions'] = 'Actions';

$il['news_property_date_label'] = 'Date';
$il['news_property_time_label'] = 'Time (HH: MM: SS)';
$il['news_property_available_label'] = 'News is active';
$il['news_property_lenta_label'] = 'Show in tape';
$il['news_property_delivery_label'] = 'Show the mailing list';
$il['news_property_rss_label'] = 'Show in RSS';
$il['news_property_header_label'] = 'Title';
$il['news_property_description_short_label'] = 'Short description';
$il['news_property_description_full_label'] = 'Full opisenie';
$il['news_property_author_label'] = 'Author';
$il['news_property_source_name_label'] = 'Source Name';
$il['news_property_source_url_label'] = 'The URL';
$il['news_property_image_label'] = 'Image';

$il['news_item_action_edit'] = 'Edit';
$il['news_item_action_remove'] = 'Remove';
$il['news_show_list_submit'] = 'OK';
$il['news_actions_with_selected'] = 'Actions with Selected:';
$il['news_submit_label'] = 'Retain';
$il['news_actions_simple'] = 'Actions';
$il['news_actions_advanced'] = 'Move to:';
$il['news_item_number'] = 'Number';
$il['news_menu_label1'] = 'The selection by date';
$il['news_select_between_label'] = 'Enter the desired date range';
$il['news_start_date'] = 'Enter the desired date range';
$il['news_end_date'] = 'Enter the desired date range';
$il['news_button_show'] = 'Display';

$il['news_property_pages_count'] = 'The number of pages in a block (N)';
$il['news_pub_pages_type'] = 'View pagination';
$il['news_pub_pages_get_block'] = 'Blocks of N pages';
$il['news_pub_pages_get_float'] = 'Tech. page is always at the center of a block of N pages';
$il['news_delete_confirm'] = 'Are you sure you want to delete the news?';

$il['news_pub_show_html_title'] = 'Show title';
$il['news_pub_show_meta_keywords'] = 'Meta-keywords';
$il['news_pub_show_meta_description'] = 'Meta-description';
$il['news_pub_pub_show_html_title_def'] = 'Title by default';
$il['news_pub_pub_show_meta_keywords_def'] = 'Meta-keywords by default';
$il['news_pub_pub_show_meta_description_def'] = 'Meta-description by default';
$il['news_property_html_title_label']       = 'Title';
$il['news_property_meta_keywords_label']    = 'Meta-keywords';
$il['news_property_meta_description_label'] = 'Meta-description';

$il['news_error_incorrect_datetime']= 'Incorrect date or time value';