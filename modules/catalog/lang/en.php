<?php
$type_langauge = 'en';

$il['catalog_show_generic_sql'] = 'Generic SQL';
$il['catalog_generic_sql_tpl'] = 'Template';
$il['catalog_generic_sql_query'] = 'SQL-query';
$il['catalog_show_export'] = 'Export';

$il['catalog_basket_plus_zero_price_label'] = '+ net price';
$il['catalog_add_variable_header_label'] = 'Adding variable';
$il['catalog_edit_variable_header_label'] = 'Edit the variable';

$il['catalog_menu_variables'] = 'variables';
$il['catalog_variables_header_title'] = 'variables';
$il['catalog_add_variable_label'] = 'Add Variable';
$il['catalog_list_variable_namedb_label'] = 'Id';
$il['catalog_list_variable_namefull_label'] = 'Name';
$il['catalog_list_variable_value_label'] = 'value';
$il['catalog_list_variable_actions'] = 'actions';
$il['catalog_variable_del_alert'] = 'Do you really want to delete the variable';

$il['catalog_variable_edit_namefull_label'] = 'Name';
$il['catalog_variable_edit_header'] = 'Edit the variable';
$il['catalog_variable_edit_namedb_label'] = 'ID <br> <small> az, 0-9, _ </ small>';
$il['catalog_variable_edit_value_label'] = 'value';
$il['catalog_variable_edit_save_label'] = 'Save';
$il['catalog_variable_save_msg_ok'] = 'Saved';
$il['catalog_variable_save_msg_incorrect_namedb'] = 'Invalid indetifikator';
$il['catalog_variable_save_msg_empty_value'] = 'Do not put the value';
$il['catalog_variable_save_msg_empty_name_full'] = 'Do not put the name';

$il['catalog_property_popular_days'] = 'Number of days for which the statistics take into account when sorting by popularity';
$il['catalog_property_popular_days_descr'] = 'Sort by popularity will work only if enabled Statistics and non-zero number of days';
$il['catalog_property_date_format'] = 'Date format';
$il['catalog_property_way_item_tpl'] = 'Template to display the product name on the road';
$il['catalog_property_way_cat_tpl'] = 'template to display the category name in the road';

$il['catalog_property_basket_page'] = 'page output basket of goods';
$il['catalog_property_compare_page'] = 'page displays comparison shopping';
$il['catalog_property_order_page'] = 'Ordering page';

$il['catalog_item_list_multi_group_tpl'] = 'Template for different groups';
$il['catalog_cats_list_tpl'] = 'Template category list';
$il['catalog_items_group_name_label'] = 'Trading group';

$il['catalog_prop_in_group_header'] = 'Show <br> in the admin';

$il['catalog_show_linked_items'] = 'Display related products';
$il['catalog_linked_items_tpl'] = 'Template list';
$il['catalog_edit_property_ismain'] = 'Main (unique)';
$il['catalog_edit_property_visibility'] = 'Visible to all groups';
$il['catalog_add_linked_item_label'] = 'Add Related Items';
$il['catalog_no_main_prop_defined_label'] = 'Do not set the main property';
$il['catalog_remove_linked_item_label'] = 'Remove from related';
$il['catalog_linked_items_list_label'] = 'Related products';
$il['catalog_linked_item_added_msg'] = 'Related item has been added';
$il['catalog_import_linked_col'] = '[Related Products]';
$il['catalog_import_linked_col_separator'] = 'separator';
$il['catalog_import_bypass_1st_line_label'] = 'Skip the first line';

$il['catalog_prop_sort_no'] = 'Unspecified';
$il['catalog_prop_sort_asc'] = 'Ascending';
$il['catalog_prop_sort_desc'] = 'Descending';

$il['catalog_group_default_cats_label'] = 'Categories of default';
$il['catalog_items_all_list_search_results_mainlabel'] = 'Search Results';
// generate a search form
$il['catalog_generate_search_form_label'] = 'Generate search box';
$il['catalog_generate_search_form_button_label'] = 'Generate';
$il['catalog_gen_search_form_enum_select_label'] = 'Select (dropdown menu one)';
$il['catalog_gen_search_form_enum_radio_label'] = 'Radio-buttons (one)';
$il['catalog_gen_search_form_enum_checkboxes_label'] = 'checkboxes (multiple values)';
$il['catalog_gen_search_form_string_accurate_label'] = 'Exact (=)';
$il['catalog_gen_search_form_string_like_label'] = 'On the entry (LIKE \"% ...%\")';
$il['catalog_gen_search_form_number_accurate_label'] = 'Exact (=)';
$il['catalog_gen_search_form_number_diapazon_label'] = 'range (> = and <=)';
$il['catalog_gen_search_form_ignore_field_label'] = 'Ignore';
$il['catalog_gen_search_form_file_select_label'] = 'Select (yes-no)';
$il['catalog_gen_search_form_file_checkbox_label'] = 'One checkbox';
$il['catalog_gen_search_form_file_radio_label'] = 'Radio-buttons (yes-no)';
$il['catalog_gen_search_form_file_necessary_label'] = 'Important';
$il['catalog_gen_search_form_file_notnecessary_label'] = 'Not important';

$il['catalog_gen_search_outfilename_label'] = '<b> The file name for the generated template </ b> <br/> <small> will be placed in / modules / catalog / templates_user / </ small>';
$il['catalog_gen_search_filtername_label'] = 'The name of the filter to be created';
$il['catalog_gen_search_filtertemplate_label'] = 'Template for the generated output filter';
$il['catalog_edit_inner_filter_showsql_label'] = 'Show the generated SQL-query';
$il['catalog_items_search_link_label'] = 'Search';

// export
$il['catalog_export_csv_menuitem'] = 'Export to CSV';
$il['catalog_export_csv_formlabel'] = 'Export to CSV';
$il['catalog_export_csv_tpl_label'] = 'Template row';
$il['catalog_export_button_label'] = 'Export';
$il['catalog_export_groups_props_label'] = 'Properties';
$il['catalog_export_filter_select_label'] = 'Internal filter';
$il['catalog_export_filter_select_all_items'] = '-without filter (all products) -';

// cart, orders and so forth
$il['catalog_show_order_fields'] = 'Fields baskets (order)';
$il['catalog_basket_order_settings_label'] = 'Settings basket';
  // the list of fields to order
$il['catalog_order_prop_list_table_label_num'] = '№';
$il['catalog_order_prop_list_table_label_name'] = 'Name';
$il['catalog_order_prop_list_table_label_namedb'] = 'Id';
$il['catalog_order_prop_list_table_label_type'] = 'type';
$il['catalog_order_prop_list_table_label_global'] = 'Required';
$il['catalog_order_prop_list_table_label_actions'] = 'actions';
$il['catalog_order_prop_list_table_label_order'] = 'Procedure';
$il['catalog_order_prop_list_table_label_change_sord'] = 'Save the order';
$il['catalog_order_prop_list_actions_del_alert'] = 'Are you sure you want to delete the';
$il['catalog_edit_property_bt_add_field'] = 'Add Field';
$il['catalog_edit_is_required_label'] = 'Required';
$il['catalog_order_field_regexp_label'] = 'regexps';
$il['catalog_order_field_saved_msg'] = 'Saved';
$il['catalog_order_templates_header'] = 'templates';
$il['catalog_order_template_basket_items_label'] = 'Template basket of goods';
$il['catalog_order_template_order_label'] = 'Template order forms';
$il['catalog_order_action_regenerate_tpls_label'] = 'Generate';
$il['catalog_order_tpls_regenerated_msg'] = 'Templates generated';
$il['catalog_order_tpls_regenerated_translit_msg'] = 'The templates are generated from translit names';
$il['catalog_order_show_order_form'] = 'Show order form';
$il['catalog_order_manager_mail_tpl'] = 'Email Template Manager';
$il['catalog_order_manager_mail_subj'] = 'The theme of the letter to the manager';
$il['catalog_order_manager_email'] = 'Email Manager';
$il['catalog_order_user_mail_tpl'] = 'Template letter to the user';
$il['catalog_order_user_mail_subj'] = 'The theme of the letter to the user';
$il['catalog_edit_order_field_label_add'] = 'Adding fields in order';
$il['catalog_edit_order_field_label_edit'] = 'Edit the field order';

// for action
$il['catalog_common_props'] = 'General Properties';
$il['catalog_show_item_name'] = 'To form the name of the element';
$il['catalog_show_inner_selection_results'] = 'To generate a sample of the inner filter';
$il['catalog_inner_selection_filter_label'] = 'Internal filter';
$il['catalog_show_item_fields_label'] = 'Template to display the names of e-ta';
$il['catalog_show_cat_fields_label'] = 'Template to display the category e-ta';


$il['catalog_show_basket_label'] = 'Display Sticker basket';
$il['catalog_show_basket_items'] = 'Display the contents of the basket';
$il['catalog_basket_label_empty_tpl'] = 'Template sticker for empty baskets';
$il['catalog_basket_label_notempty_tpl'] = 'Template sticker for a basket with goods';
$il['catalog_basket_items_tpl'] = 'Template basket of goods';

$il['catalog_inner_filter_test'] = 'filter test';

$il['catalog_inner_filters'] = 'Internal filters';
$il['catalog_list_inner_filter_actions'] = 'actions';
$il['catalog_list_inner_filter_stringid_label'] = 'ID';
$il['catalog_list_inner_filter_name_label'] = 'Name';
$il['catalog_add_inner_filter_label'] = 'Add internal filter';
$il['catalog_inner_filter_actions_del_alert'] = 'Are you sure you want to remove the internal filter';
$il['catalog_edit_inner_filter_name_label'] = 'Name';
$il['catalog_edit_inner_filter_stringid_label'] = 'String ID';
$il['catalog_edit_inner_filter_query_label'] = 'Query';
$il['catalog_edit_inner_filter_template_label'] = 'Template';
$il['catalog_edit_inner_filter_limit_label'] = 'limit';
$il['catalog_edit_inner_filter_perpage_label'] = 'Items per page';
$il['catalog_edit_inner_filter_maxpages_label'] = 'Max. number of pages in the block ';
$il['catalog_edit_inner_filter_groupid_label'] = 'Trading group filter';
$il['catalog_edit_inner_filter_targetpage_label'] = 'Product Page';
$il['catalog_edit_inner_filter_categories_label'] = 'categories';
$il['catalog_edit_inner_filter_label_add'] = 'Adding inner filter';
$il['catalog_edit_inner_filter_label_edit'] = 'Edit the inner filter';
$il['catalog_edit_inner_filter_action_save'] = 'Save the filter';
$il['catalog_edit_inner_filter_all_categories_label'] = 'All categories';
$il['catalog_edit_inner_filter_current_category_label'] = 'Current category';
$il['catalog_select_categories_label'] = 'Select a category';
$il['catalog_edit_inner_filter_save_msg_ok'] = 'Internal filter saved';
$il['catalog_edit_inner_filter_save_msg_error'] = 'Error saving inner filter';
$il['catalog_edit_inner_filter_save_msg_emptyfields'] =' Do not fill in the required fields';
$il['catalog_edit_inner_filter_save_msg_stringid_exists'] = 'String ID already exists';
$il['catalog_filter_all_groups'] = 'All groups (detect automatically)';

$il['catalog_items_page'] = 'Page to display the list of goods';

// import
$il['catalog_import_csv_successfull_msg'] = 'Import finished';
$il['catalog_import_csv_menuitem'] = 'Import from CSV';
$il['catalog_import_csv_formlabel'] = 'Import from CSV';
$il['catalog_import_file_label'] = 'File';
$il['catalog_import_textarea_label'] = 'Clipboard';
$il['catalog_import_separator_label'] = 'separator';
$il['catalog_import_separator_tab'] = 'Tab';
$il['catalog_import_separator_comma'] = 'Comma';
$il['catalog_import_separator_semicolon'] = 'semicolon';
$il['catalog_import_ignore_column_label'] = 'Ignore';
$il['catalog_import2group_label'] = 'The comrade. group ';
$il['catalog_import2cat_label'] = 'The category';
$il['catalog_new_import2cat_label'] = 'Category for the new (optional)';
$il['catalog_cat_not_selected_label'] = 'not selected';
$il['catalog_import_label'] = 'Import';
$il['catalog_import_uniq_field_label'] = 'unique field';

$il['catalog_base_name'] = 'catalog';
$il['catalog_modul_base_name1'] = 'catalog';

$il['catalog_property_items_per_page_admin'] = 'Elements on page AI';

$il['catalog_show_cats'] = 'Create a list of the categories';
$il['catalog_show_items'] = 'Create a list of goods';
$il['catalog_tpl'] = 'Template';
$il['catalog_items_list_tpl'] = 'Template list of goods';

$il['catalog_cats_list_tpl'] = 'Template category list';

$il['catalog_items_per_page_label'] = 'Items per page';
$il['catalog_show_cats_if_empty_items_label'] = 'Whether or not the list of categories of goods if not?';

// Types of available fields, catalog_prop_type _ ??? Where ??? property identifier.
$il['catalog_prop_type_text'] = 'Text';
$il['catalog_prop_type_string'] = 'string';
$il['catalog_prop_type_html'] = 'HTML';
$il['catalog_prop_type_date'] = 'date-time';
$il['catalog_prop_type_file'] = 'File';
$il['catalog_prop_type_pict'] = 'image';
$il['catalog_prop_type_number'] = 'numeric';
$il['catalog_prop_type_enum'] = 'Set values ​​(ENUM)';
$il['catalog_prop_type_enum_notselect'] = 'Not Set';
$il['catalog_prop_type_set'] = 'Set values ​​(SET)';

// Menu
$il['catalog_menu_label'] = 'Setting the directory';
$il['catalog_menu_all_props'] = 'The general properties of groups';
$il['catalog_menu_label_cats'] = 'categories';
$il['catalog_menu_groups'] = 'Product group properties';
$il['catalog_menu_items'] = 'Goods';
$il['catalog_menu_cat_props'] = 'Properties category';
$il['catalog_menu_label_import_export'] = 'Import-Export';

// List property group
$il['catalog_add_group_label'] = 'Create a new group';
$il['catalog_list_group_number_label'] = '№';
$il['catalog_list_group_name_label'] = 'Name of the group';
$il['catalog_list_group_id_label'] = 'Id';
$il['catalog_list_group_id_action'] = 'actions';
$il['catalog_list_group_action_edit'] = 'Edit';
$il['catalog_list_group_action_add_item'] = 'Add item';
$il['catalog_list_group_action_add_prop'] = 'Properties of';
$il['catalog_list_group_action_create_amin_template'] = 'Create the admin templates';
$il['catalog_list_group_action_create_amin_template_alert'] = 'If there already exists a template for this commodity group, it will be overwritten';

// Form edit / add a new group
$il['catalog_edit_group_label_edit'] = 'Edit the commodity group';
$il['catalog_edit_group_label_add'] = 'The parameters of a new product group';
$il['catalog_edit_group_name_label'] = 'Name of the group';
$il['catalog_edit_group_id_label'] = 'Group ID (Latin)';
$il['catalog_edit_group_template_list_label'] = 'Template list of goods';
$il['catalog_edit_group_template_item_label'] = 'Template cards of goods';
$il['catalog_edit_group_action_save'] = 'Save';
$il['catalog_edit_group_save_msg_ok'] = 'Trading group saved';
$il['catalog_edit_group_save_msg_err'] = 'Error saving';
$il['catalog_edit_group_action_new_template'] = 'New templates';
$il['catalog_edit_group_action_new_template_alert'] = 'This will create new patterns for this product group. <br> WARNING! Existing patterns will be overwritten and all changes to them are lost. Continue?';

// List of properties of a group
$il['catalog_group_prop_list_mainlabel'] = 'List of the properties of the product group';
$il['catalog_group_prop_list_mainlabel_global'] = 'List of common property';

$il['catalog_group_prop_list_table_label_num'] = '№';
$il['catalog_group_prop_list_table_label_name'] = 'Name';
$il['catalog_group_prop_list_table_label_namedb'] = 'Id';
$il['catalog_group_prop_list_table_label_type'] = 'type';
$il['catalog_group_prop_list_table_label_global'] = 'General';
$il['catalog_group_prop_list_table_label_actions'] = 'actions';
$il['catalog_group_prop_list_table_label_order'] = 'Procedure';
$il['catalog_group_prop_list_table_label_change_sord'] = 'Save the order and visibility';
$il['catalog_group_prop_list_actions_del_alert'] = 'Are you sure you want to delete the';

// Form edit / add properties
$il['catalog_edit_property_label_globalprop'] = 'Settings common property';
$il['catalog_edit_property_label_group'] = 'Settings property group';
$il['catalog_edit_property_label_globalprop_add'] = 'The new common property';
$il['catalog_edit_property_label_group_add'] = 'The new group properties';
$il['catalog_edit_property_name'] = 'Name';
$il['catalog_edit_property_namedb'] = 'Id';
$il['catalog_edit_property_sorted'] = 'Sorting / selection';
$il['catalog_edit_property_type'] = 'Type of property';
$il['catalog_edit_property_show_in_list'] = 'appear in the list of goods administrative interface';
$il['catalog_edit_property_type'] = 'Type of property';
$il['catalog_edit_property_bt_save'] = 'Save';
$il['catalog_edit_property_bt_add_prop'] = 'Add Property';
$il['catalog_edit_property_added_msg'] = 'added property';
$il['catalog_edit_property_saved_msg'] = 'property saved';
$il['catalog_edit_property_enum_label'] = 'Possible values';
$il['catalog_edit_property_enum_help'] = '(each value on a new line)';
$il['catalog_edit_property_enum_val_add_label'] = 'new value to the set';
$il['catalog_edit_property_enum_val_add_bt'] = 'Add';
$il['catalog_edit_property_enum_del_alert'] = 'Are you sure you want to delete the value';
$il['catalog_edit_property_enum_add_msg'] = 'The new value added';

$il['catalog_edit_property_pict_path'] = 'Path to save images';

// Work with a list of properties for categories
$il['catalog_edit_property_cat_mainlabel'] = 'The properties of all categories of the directory';
$il['catalog_edit_property_cat_num_label'] = '#';
$il['catalog_edit_property_cat_name_label'] = 'Name';
$il['catalog_edit_property_cat_dbname_label'] = 'Id';
$il['catalog_edit_property_cat_type_label'] = 'Type of property';
$il['catalog_edit_property_cat_action_label'] = 'actions';
$il['catalog_edit_property_cat_action_alert_del'] = 'Do you really want to delete the';
$il['catalog_edit_property_cat_add_prop_label'] = 'Add a property in the category';
$il['catalog_edit_property_cat_add_name_label'] = 'Name';
$il['catalog_edit_property_cat_add_dbname_label'] = 'Id';
$il['catalog_edit_property_cat_add_type_label'] = 'Type of property';
$il['catalog_edit_property_cat_add_enum_label'] = 'Possible values';
$il['catalog_edit_property_cat_add_enum_help'] = '(each value on a new line)';
$il['catalog_edit_property_cat_add_new_prop'] = 'Add Property';
$il['catalog_edit_property_cat_add_new_prop_msg'] = 'The new property is added';
$il['catalog_edit_property_cat_edit_label_main'] = 'Edit category properties';
$il['catalog_edit_property_cat_edit_name_label'] = 'Name';
$il['catalog_edit_property_cat_edit_dbname_label'] = 'Id';
$il['catalog_edit_property_cat_edit_type_label'] = 'Type of property';
$il['catalog_edit_property_cat_edit_bt_save'] = 'Save';
$il['catalog_edit_property_cat_enum_label'] = 'Possible values';
$il['catalog_edit_property_cat_enum_addnew'] = 'Add value';
$il['catalog_edit_property_cat_enum_addnew_bt'] = 'Add';
$il['catalog_edit_property_cat_enum_addnew_msg'] = 'The new property value added';
$il['catalog_edit_property_cat_enum_del_alert'] = 'Are you sure you want to delete a value from the set';

// Edit the category parameters
$il['catalog_edit_category_label_main'] = 'edit category';
$il['catalog_edit_category_isdefault_label'] = 'Category default';
$il['catalog_edit_category_need_select_label'] = 'value is selected';
$il['catalog_edit_category_bt_save_label'] = 'Save';
$il['catalog_edit_category_prop_file_del_alert'] = 'Are you sure you want to delete the downloaded file';

// List of all products
$il['catalog_items_all_list_filter_mainlabel'] = 'List of all goods store';
$il['catalog_items_all_list_filter_items_group'] = 'Show products';
$il['catalog_items_all_list_filter_all_group'] = 'included in all the groups';
$il['catalog_items_all_list_filter_no_cat'] = 'no category';
$il['catalog_items_all_list_table_num'] = '№';
$il['catalog_items_all_list_table_action'] = 'actions';
$il['catalog_items_all_list_table_action_del_sel'] = 'Remove selected';
$il['catalog_items_all_list_table_action_no'] = 'Unspecified';
$il['catalog_items_all_list_table_action_with_sel'] = 'actions marked with';
$il['catalog_items_all_list_table_action_with_sel_go'] = 'Run';
$il['catalog_items_all_list_table_action_check_all'] = 'Mark';
$il['catalog_items_all_list_table_action_uncheck_all'] = 'Remove all';

$il['catalog_items_all_list_table_group'] = 'belongs to the group';
$il['catalog_items_all_list_table_action_alertdel'] = 'Are you sure you want to delete the item';
$il['catalog_items_all_list_table_action_edit'] = 'Edit product';
$il['catalog_items_all_list_nogroups_msg'] = 'To add the item to need at least one product group';
$il['catalog_items_all_list_add_item_label'] = 'New product group';
$il['catalog_items_all_list_add_item_bt_label'] = 'Add';
$il['catalog_items_all_list_no_items'] = 'There are no products in the selected group';
$il['catalog_items_all_list_table_checkbox'] = 'Collective processing';
$il['catalog_items_all_list_table_label_save_fields'] = 'Save the field';

// The content of the categories
$il['catalog_list_category_edit_label'] = 'Edit the category';
$il['catalog_list_category_additem_label'] = 'Add item';
$il['catalog_list_category_add_item_label'] = 'New product group';
$il['catalog_list_category_add_item_bt_label'] = 'Add';
$il['catalog_list_category_item_list_label'] = 'Products in this category';
$il['catalog_list_category_item_list_notitems'] = 'In the category of goods No';
$il['catalog_list_category_item_action_edit'] = 'Edit product';
$il['catalog_list_category_not_select_cat'] = 'Category contains nothing';
$il['catalog_list_category_item_table_num'] = 'Collective processing';
$il['catalog_list_category_item_table_action'] = 'actions';
$il['catalog_list_category_item_table_order_num'] = 'Sort order';
$il['catalog_list_category_item_table_check_all'] = 'Mark';
$il['catalog_list_category_item_table_uncheck_all'] = 'Remove all';
$il['catalog_list_category_item_table_action_selected'] = 'Actions with selected:';
$il['catalog_list_category_item_table_action_selected0'] = 'Unspecified';
$il['catalog_list_category_item_table_action_selected1'] = 'Delete the category from the current';
$il['catalog_list_category_item_table_action_selected2'] = 'Move to the category';
$il['catalog_list_category_item_table_action_selected3'] = 'Delete';
$il['catalog_list_category_item_table_action_run'] = 'Run';
$il['catalog_list_category_item_table_action_change_sord'] = 'Change';
$il['catalog_list_category_item_table_alertdel'] = 'Do you really want to completely destroy the goods';
$il['catalog_list_category_item_table_alertdel_alt'] = 'Destroy goods';

// Editing form of goods
$il['catalog_item_edit_mainlabel'] = 'Edit the properties of the goods';
$il['catalog_item_edit_table_nameprop_label'] = 'The name of the';
$il['catalog_item_edit_table_value_label'] = 'Property Value';
$il['catalog_item_edit_available_label'] = 'Show item in shop';
$il['catalog_item_edit_file_del_alert'] = 'Are you sure you want to delete the downloaded file';
$il['catalog_item_edit_bt_save_label'] = 'Save changes';
$il['catalog_item_edit_incategories_label'] = 'Included in this category:';
$il['catalog_item_edit_pict_add_mark_for_big'] = 'Watermark for more images';
$il['catalog_item_edit_pict_add_mark_for_source'] = 'watermark to the image IHODNOMU';

$il['catalog_clear_label'] = 'Delete';

$il['catalog_group_name_label'] = 'Name';
$il['catalog_group_id_label'] = 'Id';
$il['catalog_group_props_label'] = 'Properties';
$il['catalog_group_namedb_label'] = 'Name (DB)';

$il['catalog_link_details_label'] = 'More ...';

// labels, messages, hints in the admin
$il['catalog_category_name_label'] = 'Category name';
$il['catalog_categories_build_from_cat_label'] = 'Category start of construction';
$il['catalog_categories_build_from_depth_label'] = 'The level of the start of construction';
$il['catalog_categories_open_levels_show_label'] = 'The amount of the disclosed menu levels';
$il['catalog_categories_max_levels_show_label'] = 'Max. the amount of output level menu';
$il['catalog_category_add_label'] = 'Add category';
$il['catalog_category_new_name'] = 'new category';
$il['catalog_category_del_alert'] = 'Do you really want to remove the category';
$il['catalog_category_remove_label'] = 'Delete category';
$il['catalog_edit_item_label'] = 'Edit Item';
$il['catalog_item_order_label'] = 'Procedure';
$il['catalog_item_name_label'] = 'Name';
$il['catalog_item_txt_label'] = 'Description';
$il['catalog_item_added_msg'] = 'item has been added';
$il['catalog_item_saved_msg'] = 'This product is saved';
$il['catalog_item_del_alert'] = 'Do you really want to remove the goods';
// $il['catalog_item_deleted_msg'] = 'The item deleted';

$il['catalog_group_items_label'] = 'goods';
$il['catalog_add_prop_label'] = 'Add Property';
$il['catalog_save_order_label'] = 'Save the order';
$il['catalog_OK_label'] = 'OK';
$il['catalog_button_show'] = 'Show';

$il['catalog_menu_label_import_commerceml'] = 'Import from CommerceML';
$il['catalog_commerceml_type'] = 'Import Type';
$il['catalog_commerceml_type_import'] = 'For information about goods (import.xml)';
$il['catalog_commerceml_type_offers'] = 'Only the prices (offers.xml)';
$il['catalog_commerceml_do_import'] = 'Import';
$il['catalog_commerceml_xmlfile'] = 'File';
$il['catalog_commerceml_itemprops_assoc'] = 'Matching fields';
$il['catalog_commerceml_name_1C'] = 'Course in 1C';
$il['catalog_commerceml_name_santa'] = 'Course in santafox';
$il['catalog_commerceml_add_assocline'] = 'Add';
$il['catalog_commerceml_price_field'] = 'Price';
$il['catalog_commerceml_price_per_field'] = 'Price';
$il['catalog_commerceml_price_per_field_no_matters'] = '- not ispolzovat--';
$il['catalog_commerceml_pricetype'] = 'The name of the price type 1C';
$il['catalog_commerceml_category'] = 'new category';
$il['catalog_commerceml_group'] = 'Tov.gruppa';
$il['catalog_commerceml_1C_ID_santafield'] = 'Property of the directory that stores the ID from 1C';
$il['catalog_commerceml_name_santafield'] = 'Property of the directory that contains the name';
$il['catalog_commerceml_error_nofile'] = '<b style = "color: red;"> Error: File not uploaded </ b>';
$il['catalog_commerceml_error_malformed_xml'] = '<b style = "color: red;"> Error: Invalid XML </ b>';
$il['catalog_commerceml_error_reqfields_not_filled'] = '<b style = "color: red;"> Error: Do not fill in the required fields </ b>';
$il['catalog_commerceml_error_no_group'] = '<b style = "color: red;"> Error: Not Found comrade. Group </ b> ';
$il['catalog_commerceml_error_no_pricetype_found'] = '<b style = "color: red;"> Error: This type of price is not found </ b>';
$il['catalog_commerceml_import_completed'] = '<b> Import completed. Added products:% added%, updated:% updated% </ b> ';
$il['catalog_commerceml_offers_completed'] = '<b> Prices updated (% updated%) </ b>';

$il['catalog_no_group_template_list'] = 'In the commodity group is not defined template output the list of goods';
$il['catalog_show_items_list_no_items'] = 'No products';
$il['catalog_delete_group_alert'] = 'Really delete comrade. group ';

$il['catalog_prop_saved_msg'] = 'property saved';
$il['catalog_quicksearch_label'] = 'Quick Search';
$il['catalog_pub_compare'] = 'Compare Products';
$il['catalog_compare_max_items'] =' Max. Number of compared items';
$il['catalog_groups_list'] = 'List of commodity groups';
$il['catalog_groups_list_delete_only_empty'] = 'Delete only possible commodity group without the goods';
$il['catalog_list_group_items_count_label'] = 'Products';
$il['catalog_edit_category_hide_from_waysite_label'] = 'Hide the road';
$il['catalog_edit_category_hide_from_site_label'] = 'Hide from site';

$il['catalog_set_type_64_max_error_msg'] = 'The data type of the SET allows you to have more than 64 possible values';
$il['catalog_not_uniq_main_prop_save'] = 'Field% fieldname% marked as unique, but non-unique value is set';
$il['catalog_onsite_th'] = 'The site';
$il['catalog_list_set_available'] = 'Show Online';
$il['catalog_list_set_unavailable'] = 'Do not show on the site';
$il['catalog_move2group_header'] = 'Move to group';
$il['catalog_move2group_do'] = 'Move';
$il['catalog_item_moved_to_group'] = 'Postponed';

$il['catalog_property_ending_basket'] = 'End have elements';
$il['catalog_property_ending_basket_descr'] = 'The correct end of the basket at the elements (separated by commas for numbers 1,2,5)';

$il['catalog_categories_build_from_cat_first_label'] = 'Current category root';