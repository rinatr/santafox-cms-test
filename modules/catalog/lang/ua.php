<?php
$type_langauge = 'ua';

$il['catalog_show_generic_sql'] = 'Generic SQL';
$il['catalog_generic_sql_tpl'] = 'Шаблон';
$il['catalog_generic_sql_query'] = 'SQL-запит';
$il['catalog_show_export'] = 'Експорт';

$il['catalog_basket_plus_zero_price_label'] = '+ договірна ціна';
$il['catalog_add_variable_header_label'] = 'Додавання змінної';
$il['catalog_edit_variable_header_label'] = 'Редагування змінної';

$il['catalog_menu_variables'] = 'Змінні';
$il['catalog_variables_header_title'] = 'Змінні';
$il['catalog_add_variable_label'] = 'Додати змінну';
$il['catalog_list_variable_namedb_label'] = 'Ідентифікатор';
$il['catalog_list_variable_namefull_label'] = 'Назва';
$il['catalog_list_variable_value_label'] = 'Значення';
$il['catalog_list_variable_actions'] = 'Дії';
$il['catalog_variable_del_alert'] = 'Дійсно хочете видалити змінну';

$il['catalog_variable_edit_namefull_label'] = 'Назва';
$il['catalog_variable_edit_header'] = 'Редагування змінної';
$il['catalog_variable_edit_namedb_label'] = 'Ідентифікатор <br> <small> az, 0-9, _ </ small>';
$il['catalog_variable_edit_value_label'] = 'Значення';
$il['catalog_variable_edit_save_label'] = 'Зберегти';
$il['catalog_variable_save_msg_ok'] = 'Збережено';
$il['catalog_variable_save_msg_incorrect_namedb'] = 'Невірний індетіфікатор';
$il['catalog_variable_save_msg_empty_value'] = 'Не введене значення';
$il['catalog_variable_save_msg_empty_name_full'] = 'Не введене назва';

$il['catalog_property_popular_days'] = 'Кількість днів, за якими враховувати статистику при сортуванні за популярністю';
$il['catalog_property_popular_days_descr'] = 'Сортування за популярністю буде працювати тільки при включеній статистиці і ненулевом к-ть днів';
$il['catalog_property_date_format'] = 'Формат дати';
$il['catalog_property_way_item_tpl'] = 'Шаблон для відображення назви товару в дорозі';
$il['catalog_property_way_cat_tpl'] = 'Шаблон для відображення назви категорії в дорозі';

$il['catalog_property_basket_page'] = 'Сторінка виведення кошика товарів';
$il['catalog_property_compare_page'] = 'Сторінка виведення порівняння товарів';
$il['catalog_property_order_page'] = 'Сторінка оформлення замовлення';

$il['catalog_item_list_multi_group_tpl'] = 'Шаблон для різних груп';
$il['catalog_cats_list_tpl'] = 'Шаблон списку категорій';
$il['catalog_items_group_name_label'] = 'Товарна група';

$il['catalog_prop_in_group_header'] = 'Відображати <br> в адмінці?';

$il['catalog_show_linked_items'] = 'Показати пов`язані товари';
$il['catalog_linked_items_tpl'] = 'Шаблон списку';
$il['catalog_edit_property_ismain'] = 'Основне (унікальне)';
$il['catalog_edit_property_visibility'] = 'Зробити видимим для всіх груп';
$il['catalog_add_linked_item_label'] = 'Додати пов`язаний товар';
$il['catalog_no_main_prop_defined_label'] = 'Не налаштовано основна властивість';
$il['catalog_remove_linked_item_label'] = 'Видалити з пов`язаних';
$il['catalog_linked_items_list_label'] = 'Пов`язані товари';
$il['catalog_linked_item_added_msg'] = 'Зв`язаний товар доданий';
$il['catalog_import_linked_col'] = '[Пов`язані товари]';
$il['catalog_import_linked_col_separator'] = 'Роздільник';
$il['catalog_import_bypass_1st_line_label'] = 'Пропустити першу сходинку';

$il['catalog_prop_sort_no'] = 'Чи не вибрано';
$il['catalog_prop_sort_asc'] = 'За зростанням';
$il['catalog_prop_sort_desc'] = 'За спаданням';

$il['catalog_group_default_cats_label'] = 'Категорії по-замовчуванню';
$il['catalog_items_all_list_search_results_mainlabel'] = 'Результати пошуку';
// генерування форми пошуку
$il['catalog_generate_search_form_label'] = 'Згенерувати форму пошуку';
$il['catalog_generate_search_form_button_label'] = 'Згенерувати';
$il['catalog_gen_search_form_enum_select_label'] = 'Select (випадаюче меню, одне значення)';
$il['catalog_gen_search_form_enum_radio_label'] = 'Radio-buttons (одне значення)';
$il['catalog_gen_search_form_enum_checkboxes_label'] = 'Чекбокси (кілька значень)';
$il['catalog_gen_search_form_string_accurate_label'] = 'Точне збіг (=)';
$il['catalog_gen_search_form_string_like_label'] = 'За входженню (LIKE "% ...%")';
$il['catalog_gen_search_form_number_accurate_label'] = 'Точне збіг (=)';
$il['catalog_gen_search_form_number_diapazon_label'] = 'Діапазон (> = і <=)';
$il['catalog_gen_search_form_ignore_field_label'] = 'Ігнорувати';
$il['catalog_gen_search_form_file_select_label'] = 'Select (так-ні)';
$il['catalog_gen_search_form_file_checkbox_label'] = 'Один чекбокс';
$il['catalog_gen_search_form_file_radio_label'] = 'Radio-buttons (так-ні)';
$il['catalog_gen_search_form_file_necessary_label'] = 'Важливо';
$il['catalog_gen_search_form_file_notnecessary_label'] = 'Не важливо';

$il['catalog_gen_search_outfilename_label'] = '<b> Файл для згенерованого шаблону </ b> <br/> <small> буде поміщений в / modules / catalog / templates_user / </ small>';
$il['catalog_gen_search_filtername_label'] = 'Назва для створюваного фільтра';
$il['catalog_gen_search_filtertemplate_label'] = 'Шаблон виводу для створюваного фільтра';
$il['catalog_edit_inner_filter_showsql_label'] = 'Показати згенерований SQL-запит';
$il['catalog_items_search_link_label'] = 'Пошук';

// для експорту
$il['catalog_export_csv_menuitem'] = 'Експорт в CSV';
$il['catalog_export_csv_formlabel'] = 'Експортувати в CSV';
$il['catalog_export_csv_tpl_label'] = 'Шаблон рядка';
$il['catalog_export_button_label'] = 'Експорт';
$il['catalog_export_groups_props_label'] = 'Властивості';
$il['catalog_export_filter_select_label'] = 'Внутрішній фільтр';
$il['catalog_export_filter_select_all_items'] = '-без фільтра (всі товари) -';

// корзина, замовлення ітп
$il['catalog_show_order_fields'] = 'Поля кошика (замовлення)';
$il['catalog_basket_order_settings_label'] = 'Налаштування кошика';
 // список полів замовлення
$il['catalog_order_prop_list_table_label_num'] = '№';
$il['catalog_order_prop_list_table_label_name'] = 'Назва';
$il['catalog_order_prop_list_table_label_namedb'] = 'Ідентифікатор';
$il['catalog_order_prop_list_table_label_type'] = 'Тип';
$il['catalog_order_prop_list_table_label_global'] = 'Обов`язкове?';
$il['catalog_order_prop_list_table_label_actions'] = 'Дії';
$il['catalog_order_prop_list_table_label_order'] = 'Порядок';
$il['catalog_order_prop_list_table_label_change_sord'] = 'Зберегти порядок';
$il['catalog_order_prop_list_actions_del_alert'] = 'Ви дійсно хочете видалити властивість';
$il['catalog_edit_property_bt_add_field'] = 'Додати поле';
$il['catalog_edit_is_required_label'] = 'Обов`язкове?';
$il['catalog_order_field_regexp_label'] = 'регекспе';
$il['catalog_order_field_saved_msg'] = 'Збережено';
$il['catalog_order_templates_header'] = 'Шаблони';
$il['catalog_order_template_basket_items_label'] = 'Шаблон товарів кошика';
$il['catalog_order_template_order_label'] = 'Шаблон форми замовлення';
$il['catalog_order_action_regenerate_tpls_label'] = 'Згенерувати';
$il['catalog_order_tpls_regenerated_msg'] = 'Шаблони згенеровані';
$il['catalog_order_tpls_regenerated_translit_msg'] = 'Шаблони згенеровані з транслітом імен';
$il['catalog_order_show_order_form'] = 'Показати форму замовлення';
$il['catalog_order_manager_mail_tpl'] = 'Шаблон листа менеджеру';
$il['catalog_order_manager_mail_subj'] = 'Тема листа менеджеру';
$il['catalog_order_manager_email'] = 'Email менеджера';
$il['catalog_order_user_mail_tpl'] = 'Шаблон листа користувачу';
$il['catalog_order_user_mail_subj'] = 'Тема листа користувачеві';
$il['catalog_edit_order_field_label_add'] = 'Додавання поля в замовлення';
$il['catalog_edit_order_field_label_edit'] = 'Редагування поля замовлення';

// для дій
$il['catalog_common_props'] = 'Загальні властивості';
$il['catalog_show_item_name'] = 'Сформувати назва елемента';
$il['catalog_show_inner_selection_results'] = 'Сформувати вибірку по внутрішньому фільтру';
$il['catalog_inner_selection_filter_label'] = 'Внутрішній фільтр';
$il['catalog_show_item_fields_label'] = 'Шаблон для виведення назви ел-та';
$il['catalog_show_cat_fields_label'] = 'Шаблон для виведення категорії ел-та';


$il['catalog_show_basket_label'] = 'Вивести стікер кошика';
$il['catalog_show_basket_items'] = 'Вивести вміст кошика';
$il['catalog_basket_label_empty_tpl'] = 'Шаблон стікера для порожньої кошика';
$il['catalog_basket_label_notempty_tpl'] = 'Шаблон стікера для кошика з товарами';
$il['catalog_basket_items_tpl'] = 'Шаблон кошика товарів';

$il['catalog_inner_filter_test'] = 'Тест фільтра';

$il['catalog_inner_filters'] = 'Внутрішні фільтри';
$il['catalog_list_inner_filter_actions'] = 'Дії';
$il['catalog_list_inner_filter_stringid_label'] = 'ID';
$il['catalog_list_inner_filter_name_label'] = 'Назва';
$il['catalog_add_inner_filter_label'] = 'Додати внутрішній фільтр';
$il['catalog_inner_filter_actions_del_alert'] = 'Ви дійсно хочете видалити внутрішній фільтр';
$il['catalog_edit_inner_filter_name_label'] = 'Назва';
$il['catalog_edit_inner_filter_stringid_label'] = 'Строковий ID';
$il['catalog_edit_inner_filter_query_label'] = 'Запит';
$il['catalog_edit_inner_filter_template_label'] = 'Шаблон';
$il['catalog_edit_inner_filter_limit_label'] = 'Ліміт';
$il['catalog_edit_inner_filter_perpage_label'] = 'Товарів на сторінку';
$il['catalog_edit_inner_filter_maxpages_label'] = 'Макс. кількість сторінок у блоці ';
$il['catalog_edit_inner_filter_groupid_label'] = 'Товарна група фільтра';
$il['catalog_edit_inner_filter_targetpage_label'] = 'Сторінка товару';
$il['catalog_edit_inner_filter_categories_label'] = 'Категорії';
$il['catalog_edit_inner_filter_label_add'] = 'Додавання внутрішнього фільтра';
$il['catalog_edit_inner_filter_label_edit'] = 'Редагування внутрішнього фільтра';
$il['catalog_edit_inner_filter_action_save'] = 'Зберегти фільтр';
$il['catalog_edit_inner_filter_all_categories_label'] = 'Всі категорії';
$il['catalog_edit_inner_filter_current_category_label'] = 'Поточна категорія';
$il['catalog_select_categories_label'] = 'Вибрати категорії';
$il['catalog_edit_inner_filter_save_msg_ok'] = 'Внутрішній фільтр збережений';
$il['catalog_edit_inner_filter_save_msg_error'] = 'Помилка при збереженні внутрішнього фільтра';
$il['catalog_edit_inner_filter_save_msg_emptyfields'] = 'Не заповнено необхідні поля';
$il['catalog_edit_inner_filter_save_msg_stringid_exists'] = 'Строковий ID вже існує';
$il['catalog_filter_all_groups'] = 'Всі групи (визначати автоматично)';

$il['catalog_items_page'] = 'Сторінка для виведення списку товарів';

// для імпорту
$il['catalog_import_csv_successfull_msg'] = 'Імпорт завершений';
$il['catalog_import_csv_menuitem'] = 'Імпортувати з CSV';
$il['catalog_import_csv_formlabel'] = 'Імпортувати з CSV';
$il['catalog_import_file_label'] = 'Файл';
$il['catalog_import_textarea_label'] = 'Буфер обміну';
$il['catalog_import_separator_label'] = 'Роздільник';
$il['catalog_import_separator_tab'] = 'Табуляция';
$il['catalog_import_separator_comma'] = 'Кома';
$il['catalog_import_separator_semicolon'] = 'Крапка з комою';
$il['catalog_import_ignore_column_label'] = 'Ігнорувати';
$il['catalog_import2group_label'] = 'У тов. групу ';
$il['catalog_import2cat_label'] = 'У категорію';
$il['catalog_new_import2cat_label'] = 'Категорія для нових (необов`язково)';
$il['catalog_cat_not_selected_label'] = 'не вибрана';
$il['catalog_import_label'] = 'Імпорт';
$il['catalog_import_uniq_field_label'] = 'Унікальне поле';

$il['catalog_base_name'] = 'Каталог товарів';
$il['catalog_modul_base_name1'] = 'Каталог товарів';

$il['catalog_property_items_per_page_admin'] = 'Елементів на сторінку в АІ';

$il['catalog_show_cats'] = 'Сформувати список категорій';
$il['catalog_show_items'] = 'Сформувати список товарів';
$il['catalog_tpl'] = 'Шаблон';
$il['catalog_items_list_tpl'] = 'Шаблон списку товарів';

$il['catalog_cats_list_tpl'] = 'Шаблон списку категорій';

$il['catalog_items_per_page_label'] = 'Товарів на сторінку';
$il['catalog_show_cats_if_empty_items_label'] = 'Виводити чи список категорій, якщо немає товарів?';

// Типи доступних полів, catalog_prop_type _ ??? , Де ??? ідентифікатор властивості.
$il['catalog_prop_type_text'] = 'Текстове';
$il['catalog_prop_type_string'] = 'Рядок';
$il['catalog_prop_type_html'] = 'HTML';
$il['catalog_prop_type_date'] = 'Дата-час';
$il['catalog_prop_type_file'] = 'Файл';
$il['catalog_prop_type_pict'] = 'Зображення';
$il['catalog_prop_type_number'] = 'Числове';
$il['catalog_prop_type_enum'] = 'Набір значень (ENUM)';
$il['catalog_prop_type_enum_notselect'] = 'не встановлено';
$il['catalog_prop_type_set'] = 'Набір значень (SET)';

// Меню
$il['catalog_menu_label'] = 'Налаштування каталогу';
$il['catalog_menu_all_props'] = 'Загальні властивості груп';
$il['catalog_menu_label_cats'] = 'Категорії';
$il['catalog_menu_groups'] = 'Товарні групи їх властивості';
$il['catalog_menu_items'] = 'Товари';
$il['catalog_menu_cat_props'] = 'Властивості категорій';
$il['catalog_menu_label_import_export'] = 'Імпорт-експорт';

// Список групи властивостей
$il['catalog_add_group_label'] = 'Створити нову групу';
$il['catalog_list_group_number_label'] = '№';
$il['catalog_list_group_name_label'] = 'Назва групи';
$il['catalog_list_group_id_label'] = 'Ідентифікатор';
$il['catalog_list_group_id_action'] = 'Дії';
$il['catalog_list_group_action_edit'] = 'Редагувати';
$il['catalog_list_group_action_add_item'] = 'Додати товар';
$il['catalog_list_group_action_add_prop'] = 'Властивості';
$il['catalog_list_group_action_create_amin_template'] = 'Створити адмінських шаблони';
$il['catalog_list_group_action_create_amin_template_alert'] = 'Якщо вже існує шаблон для цієї товарної групи, то він буде переписаний';

// Форма редагування / додавання нової групи
$il['catalog_edit_group_label_edit'] = 'Редагування товарної групи';
$il['catalog_edit_group_label_add'] = 'Параметри нової товарної групи';
$il['catalog_edit_group_name_label'] = 'Назва групи';
$il['catalog_edit_group_id_label'] = 'Ідентифікатор групи (латиницею)';
$il['catalog_edit_group_template_list_label'] = 'Шаблон списку товарів';
$il['catalog_edit_group_template_item_label'] = 'Шаблон картки товару';
$il['catalog_edit_group_action_save'] = 'Зберегти';
$il['catalog_edit_group_save_msg_ok'] = 'Товарна група збережена';
$il['catalog_edit_group_save_msg_err'] = 'Помилка при збереженні';
$il['catalog_edit_group_action_new_template'] = 'Створити шаблони';
$il['catalog_edit_group_action_new_template_alert'] = 'Будуть створені нові шаблони для цієї товарної групи. <br> УВАГА! Існуючі шаблони будуть переписані і всі зміни в них втрачені. Продовжити? ';

// Список властивостей у групи
$il['catalog_group_prop_list_mainlabel'] = 'Список властивостей товарної групи';
$il['catalog_group_prop_list_mainlabel_global'] = 'Список загальних властивостей';

$il['catalog_group_prop_list_table_label_num'] = '№';
$il['catalog_group_prop_list_table_label_name'] = 'Назва';
$il['catalog_group_prop_list_table_label_namedb'] = 'Ідентифікатор';
$il['catalog_group_prop_list_table_label_type'] = 'Тип';
$il['catalog_group_prop_list_table_label_global'] = 'Загальна';
$il['catalog_group_prop_list_table_label_actions'] = 'Дії';
$il['catalog_group_prop_list_table_label_order'] = 'Порядок';
$il['catalog_group_prop_list_table_label_change_sord'] = 'Зберегти порядок і видимість';
$il['catalog_group_prop_list_actions_del_alert'] = 'Ви дійсно хочете видалити властивість';

// Форма редагування / додавання властивості
$il['catalog_edit_property_label_globalprop'] = 'Параметри загальної властивості';
$il['catalog_edit_property_label_group'] = 'Параметри властивості групи';
$il['catalog_edit_property_label_globalprop_add'] = 'Нове загальна властивість';
$il['catalog_edit_property_label_group_add'] = 'Нове властивості групи';
$il['catalog_edit_property_name'] = 'Назва';
$il['catalog_edit_property_namedb'] = 'Ідентифікатор';
$il['catalog_edit_property_sorted'] = 'Сортування / відбір';
$il['catalog_edit_property_type'] = 'Тип властивості';
$il['catalog_edit_property_show_in_list'] = 'Виводити у списку товарів адміністративного інтерфейсу';
$il['catalog_edit_property_type'] = 'Тип властивості';
$il['catalog_edit_property_bt_save'] = 'Зберегти';
$il['catalog_edit_property_bt_add_prop'] = 'Додати властивість';
$il['catalog_edit_property_added_msg'] = 'Властивість додано';
$il['catalog_edit_property_saved_msg'] = 'Властивість збережено';
$il['catalog_edit_property_enum_label'] = 'Можливі значення';
$il['catalog_edit_property_enum_help'] = '(кожне значення починається з нового рядка)';
$il['catalog_edit_property_enum_val_add_label'] = 'Нове значення в набір';
$il['catalog_edit_property_enum_val_add_bt'] = 'Додати';
$il['catalog_edit_property_enum_del_alert'] = 'Ви дійсно хочете видалити значення';
$il['catalog_edit_property_enum_add_msg'] = 'Нове значення додано';

$il['catalog_edit_property_pict_path'] = 'Шлях збереження зображень';

// Робота зі списком властивостей для категорій
$il['catalog_edit_property_cat_mainlabel'] = 'Властивості всіх категорій каталогу';
$il['catalog_edit_property_cat_num_label'] = '№';
$il['catalog_edit_property_cat_name_label'] = 'Назва';
$il['catalog_edit_property_cat_dbname_label'] = 'Ідентифікатор';
$il['catalog_edit_property_cat_type_label'] = 'Тип властивості';
$il['catalog_edit_property_cat_action_label'] = 'Дії';
$il['catalog_edit_property_cat_action_alert_del'] = 'Ви дійсно хочете видалити властивість';
$il['catalog_edit_property_cat_add_prop_label'] = 'Додати властивість в категорії';
$il['catalog_edit_property_cat_add_name_label'] = 'Назва';
$il['catalog_edit_property_cat_add_dbname_label'] = 'Ідентифікатор';
$il['catalog_edit_property_cat_add_type_label'] = 'Тип властивості';
$il['catalog_edit_property_cat_add_enum_label'] = 'Можливі значення';
$il['catalog_edit_property_cat_add_enum_help'] = '(кожне значення починається з нового рядка)';
$il['catalog_edit_property_cat_add_new_prop'] = 'Додати властивість';
$il['catalog_edit_property_cat_add_new_prop_msg'] = 'Нова властивість додано';
$il['catalog_edit_property_cat_edit_label_main'] = 'Редагування властивості категорій';
$il['catalog_edit_property_cat_edit_name_label'] = 'Назва';
$il['catalog_edit_property_cat_edit_dbname_label'] = 'Ідентифікатор';
$il['catalog_edit_property_cat_edit_type_label'] = 'Тип властивості';
$il['catalog_edit_property_cat_edit_bt_save'] = 'Зберегти';
$il['catalog_edit_property_cat_enum_label'] = 'Можливі значення';
$il['catalog_edit_property_cat_enum_addnew'] = 'Додати значення';
$il['catalog_edit_property_cat_enum_addnew_bt'] = 'Додати';
$il['catalog_edit_property_cat_enum_addnew_msg'] = 'Нове значення властивості додано';
$il['catalog_edit_property_cat_enum_del_alert'] = 'Ви дійсно хочете видалити значення з набору';

// Редагуємо параметри категорії
$il['catalog_edit_category_label_main'] = 'Редагування категорії';
$il['catalog_edit_category_isdefault_label'] = 'Категорія за замовчуванням';
$il['catalog_edit_category_need_select_label'] = 'Значення не вибрано';
$il['catalog_edit_category_bt_save_label'] = 'Зберегти';
$il['catalog_edit_category_prop_file_del_alert'] = 'Ви дійсно хочете видалити завантажений файл';

// Список всіх товарів
$il['catalog_items_all_list_filter_mainlabel'] = 'Список всіх товарів магазину';
$il['catalog_items_all_list_filter_items_group'] = 'Показати товари';
$il['catalog_items_all_list_filter_all_group'] = 'входять в усі групи';
$il['catalog_items_all_list_filter_no_cat'] = 'без категорії';
$il['catalog_items_all_list_table_num'] = '№';
$il['catalog_items_all_list_table_action'] = 'Дії';
$il['catalog_items_all_list_table_action_del_sel'] = 'Видалити вибрані';
$il['catalog_items_all_list_table_action_no'] = 'Чи не вибрано';
$il['catalog_items_all_list_table_action_with_sel'] = 'Дії з відзначеними';
$il['catalog_items_all_list_table_action_with_sel_go'] = 'Виконати';
$il['catalog_items_all_list_table_action_check_all'] = 'Відзначити всі';
$il['catalog_items_all_list_table_action_uncheck_all'] = 'Зняти всі';

$il['catalog_items_all_list_table_group'] = 'Належить групі';
$il['catalog_items_all_list_table_action_alertdel'] = 'Ви дійсно хочете видалити товар';
$il['catalog_items_all_list_table_action_edit'] = 'Редагувати товар';
$il['catalog_items_all_list_nogroups_msg'] = 'Щоб додати товар потрібна хоча б одна товарна група';
$il['catalog_items_all_list_add_item_label'] = 'Новий товар групи';
$il['catalog_items_all_list_add_item_bt_label'] = 'Додати';
$il['catalog_items_all_list_no_items'] = 'Немає товарів в обраній групі';
$il['catalog_items_all_list_table_checkbox'] = 'Групова обробка';
$il['catalog_items_all_list_table_label_save_fields'] = 'Зберегти поля';

// Зміст категорій
$il['catalog_list_category_edit_label'] = 'Редагувати категорію';
$il['catalog_list_category_additem_label'] = 'Додати товар';
$il['catalog_list_category_add_item_label'] = 'Новий товар групи';
$il['catalog_list_category_add_item_bt_label'] = 'Додати';
$il['catalog_list_category_item_list_label'] = 'Товари цієї категорії';
$il['catalog_list_category_item_list_notitems'] = 'У категорії немає жодного товару';
$il['catalog_list_category_item_action_edit'] = 'Редагувати товар';
$il['catalog_list_category_not_select_cat'] = 'Категорія нічого не містить';
$il['catalog_list_category_item_table_num'] = 'Групова обробка';
$il['catalog_list_category_item_table_action'] = 'Дії';
$il['catalog_list_category_item_table_order_num'] = 'Порядок сортування';
$il['catalog_list_category_item_table_check_all'] = 'Відзначити всі';
$il['catalog_list_category_item_table_uncheck_all'] = 'Зняти всі';
$il['catalog_list_category_item_table_action_selected'] = 'Дії з відзначеними:';
$il['catalog_list_category_item_table_action_selected0'] = 'Чи не вибрано';
$il['catalog_list_category_item_table_action_selected1'] = 'Виключити з поточної категорії';
$il['catalog_list_category_item_table_action_selected2'] = 'Перенести в категорію';
$il['catalog_list_category_item_table_action_selected3'] = 'ВИДАЛИТИ';
$il['catalog_list_category_item_table_action_run'] = 'Виконати';
$il['catalog_list_category_item_table_action_change_sord'] = 'Змінити';
$il['catalog_list_category_item_table_alertdel'] = 'Ви дійсно хочете ПОВНІСТЮ знищити товар';
$il['catalog_list_category_item_table_alertdel_alt'] = 'Знищити товар';

// Форма редагування товару
$il['catalog_item_edit_mainlabel'] = 'Редагуємо властивості товару';
$il['catalog_item_edit_table_nameprop_label'] = 'Назва властивості';
$il['catalog_item_edit_table_value_label'] = 'Значення властивості';
$il['catalog_item_edit_available_label'] = 'Відображати товар у магазині';
$il['catalog_item_edit_file_del_alert'] = 'Ви дійсно хочете видалити завантажений файл';
$il['catalog_item_edit_bt_save_label'] = 'Зберегти зміни';
$il['catalog_item_edit_incategories_label'] = 'Входить в категорії:';
$il['catalog_item_edit_pict_add_mark_for_big'] = 'Водяний знак до ВЕЛИКОМУ зображенню';
$il['catalog_item_edit_pict_add_mark_for_source'] = 'Водяний знак до ІХОДНОМУ зображенню';

$il['catalog_clear_label'] = 'Очистити';

$il['catalog_group_name_label'] = 'Назва';
$il['catalog_group_id_label'] = 'Ідентифікатор';
$il['catalog_group_props_label'] = 'Властивості';
$il['catalog_group_namedb_label'] = 'Назва (БД)';

$il['catalog_link_details_label'] = 'Докладніше ...';

// лейбли, повідомлення, хинти в адмінці
$il['catalog_category_name_label'] = 'Назва категорії';
$il['catalog_categories_build_from_cat_label'] = 'Категорія початку побудови';
$il['catalog_categories_build_from_depth_label'] = 'Рівень початку побудови';
$il['catalog_categories_open_levels_show_label'] = 'Кількість розкритих рівнів меню';
$il['catalog_categories_max_levels_show_label'] = 'Макс. кількість виведених рівнів меню ';
$il['catalog_category_add_label'] = 'Додати категорію';
$il['catalog_category_new_name'] = 'Нова категорія';
$il['catalog_category_del_alert'] = 'Дійсно хочете видалити категорію';
$il['catalog_category_remove_label'] = 'Видалити категорію';
$il['catalog_edit_item_label'] = 'Редагування товару';
$il['catalog_item_order_label'] = 'Порядок';
$il['catalog_item_name_label'] = 'Назва';
$il['catalog_item_txt_label'] = 'Опис';
$il['catalog_item_added_msg'] = 'Товар доданий';
$il['catalog_item_saved_msg'] = 'Товар збережений';
$il['catalog_item_del_alert'] = 'Дійсно хочете видалити товар';
// $il['catalog_item_deleted_msg'] = 'Товар вилучений';

$il['catalog_group_items_label'] = 'Товари';
$il['catalog_add_prop_label'] = 'Додати властивість';
$il['catalog_save_order_label'] = 'Зберегти порядок';
$il['catalog_OK_label'] = 'OK';
$il['catalog_button_show'] = 'Показати';

$il['catalog_menu_label_import_commerceml'] = 'Імпорт з CommerceML';
$il['catalog_commerceml_type'] = 'Тип імпорту';
$il['catalog_commerceml_type_import'] = 'Відомості про товари (import.xml)';
$il['catalog_commerceml_type_offers'] = 'Тільки ціни (offers.xml)';
$il['catalog_commerceml_do_import'] = 'Імпортувати';
$il['catalog_commerceml_xmlfile'] = 'Файл';
$il['catalog_commerceml_itemprops_assoc'] = 'Відповідності полів';
$il['catalog_commerceml_name_1C'] = 'Поле в 1С';
$il['catalog_commerceml_name_santa'] = 'Поле в santafox';
$il['catalog_commerceml_add_assocline'] = 'Додати';
$il['catalog_commerceml_price_field'] = 'Ціна';
$il['catalog_commerceml_price_per_field'] = 'Ціна за';
$il['catalog_commerceml_price_per_field_no_matters'] = '- НЕ іспользовать--';
$il['catalog_commerceml_pricetype'] = 'Назва типу ціни в 1С';
$il['catalog_commerceml_category'] = 'Категорія для нових';
$il['catalog_commerceml_group'] = 'Тов.группа';
$il['catalog_commerceml_1C_ID_santafield'] = 'Властивість каталогу, в якому зберігається ID з 1С';
$il['catalog_commerceml_name_santafield'] = 'Властивість каталогу, в якому зберігається назва';
$il['catalog_commerceml_error_nofile'] = '<b style = "color: red;"> Помилка: Файл не завантажений </ b>';
$il['catalog_commerceml_error_malformed_xml'] = '<b style = "color: red;"> Помилка: Некоректний XML </ b>';
$il['catalog_commerceml_error_reqfields_not_filled'] = '<b style = "color: red;"> Помилка: Чи не заповнені обов`язкові поля </ b>';
$il['catalog_commerceml_error_no_group'] = '<b style = "color: red;"> Помилка: Чи не знайдена тов. група </ b> ';
$il['catalog_commerceml_error_no_pricetype_found'] = '<b style = "color: red;"> Помилка: Зазначений тип ціни не знайдений </ b>';
$il['catalog_commerceml_import_completed'] = '<b> Імпорт завершений. Додано товарів:% added%, поновлено:% updated% </ b> ';
$il['catalog_commerceml_offers_completed'] = '<b> Ціни оновлені (% updated%) </ b>';

$il['catalog_no_group_template_list'] = 'У товарної групи не визначений шаблон виведення списку товарів';
$il['catalog_show_items_list_no_items'] = 'Немає товарів';
$il['catalog_delete_group_alert'] = 'Дійсно видалити тов. групу ';

$il['catalog_prop_saved_msg'] = 'Властивість збережено';
$il['catalog_quicksearch_label'] = 'Швидкий пошук';
$il['catalog_pub_compare'] = 'Порівняння товарів';
$il['catalog_compare_max_items'] = 'Макс. кол-во порівнюваних товарів ';
$il['catalog_groups_list'] = 'Список товарних груп';
$il['catalog_groups_list_delete_only_empty'] = 'Видалити можливо тільки товарну групу без товарів';
$il['catalog_list_group_items_count_label'] = 'Товарів';
$il['catalog_edit_category_hide_from_waysite_label'] = 'Приховувати в дорозі';
$il['catalog_edit_category_hide_from_site_label'] = 'Не показувати на сайті';

$il['catalog_set_type_64_max_error_msg'] = 'Тип даних SET не дозволяє мати більш 64 можливих значень';
$il['catalog_not_uniq_main_prop_save'] = 'Поле% fieldname% позначено як унікальне, але встановлено неунікальне значення';
$il['catalog_onsite_th'] = 'На сайті?';
$il['catalog_list_set_available'] = 'Показувати на сайті';
$il['catalog_list_set_unavailable'] = 'Не відображати на сайті';
$il['catalog_move2group_header'] = 'Перенести в групу';
$il['catalog_move2group_do'] = 'Перенести';
$il['catalog_item_moved_to_group'] = 'Перенесено';

$il['catalog_property_ending_basket'] = 'Закінчення у елементів';
$il['catalog_property_ending_basket_descr'] = 'Правильні закінчення в кошику у елементів (через кому для цифр 1,2,5)';

$il['catalog_categories_build_from_cat_first_label'] = 'Поточна категорія коренева';