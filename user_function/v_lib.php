<?php
/**
 * @param string $name
 * @param string $default
 * @return null|string
 */
function v_request($name,$default=''){
    if(isset($_REQUEST[$name]))
        return $_REQUEST[$name];
    else
        return "".$default;
}

/**
 * Выводит ' selected="selected"' если $_REQUEST[$name]==$v
 * @param $name
 * @param $value
 * @return string
 */
function v_selected_print($name,$value){
    if(isset($_REQUEST[$name]) && $_REQUEST[$name]==$value) return ' selected="selected"'; else return '';
}

/**
 * Выводит checked="checked" если $_REQUEST[$name]==$v
 * @param $name
 * @param $value
 * @return string
 */
function v_checked_print($name,$value){
    if(isset($_REQUEST[$name]) && $_REQUEST[$name]==$value) return ' checked="checked"'; else return '';
}