<?php

class backup
{
    private $root;
    private $path_content = array("/content");
    private $path_system = array(
        "/admin",
        "/include",
        "/modules"
    );
    private $path_design = array("/design");
    private $path_save;
    private $debug_trace;

    function __construct()
    {
        $this->root = dirname(dirname(__FILE__));
        $this->path_save = $this->root."/backup/";
    }

    function error_last_get()
    {
        return $this->debug_trace;
    }


    function error_last_set($data)
    {
        $this->debug_trace = $data;
    }

    function get_file_prefix4rule(array $rule)
    {
        return 'backup-'.$rule['stringid']."-";
    }

    function backup_restore($id, $file_index)
    {
        global $kernel;
        //В качестве $id нам могут передать имя файла, либо ID записи
        if (intval($id)>0)
        {
            $rule = $kernel->db_get_record_simple("_backup","id=".$id);
            if(!$rule)
                return false;
            $real_filename = $rule['real_filename'];
        }
        elseif (file_exists($this->path_save.$id))
            $real_filename = $id;
        else
            return false;

		require_once(dirname(dirname(__FILE__))."/components/pclzip/pclzip.lib.php");

		$full_zip_path = $this->path_save.$real_filename;
		$archive = new PclZip($full_zip_path);
		$list = $archive->listContent();
		if ($list == 0) //ошибка чтения архива
		{
		    $kernel->pub_console_show("open zip error: ".$archive->errorName().", file: ".$full_zip_path);
		    flush();
		    $this->error_last_set("open zip error: ".$archive->errorName().", file: ".$full_zip_path);
		    return false;
		}
		$total = count($list);

		if ($total == $file_index)
		{
		    $kernel->pub_console_show("Done!");
		    flush();
		    return true;
		}

        $afile = $list[$file_index];
		if ($file_index == 0)
		    $need_santa_root_check = true;
		else
		    $need_santa_root_check = false;


	    ini_set('max_execution_time', 180);


	    $list1 = $archive->extract(PCLZIP_OPT_BY_INDEX, array($file_index), PCLZIP_OPT_EXTRACT_AS_STRING);
	    if ($list1 == 0)
	    {
	        $this->error_last_set("extract zip error: ".$archive->errorName());
	        $kernel->pub_console_show("extract zip error: ".$archive->errorName());
	        flush();
	        return false;
	    }

	    $kernel->pub_console_show('file: '.$afile['stored_filename']);
	    flush();

	    $file_content = $list1[0]['content'];

	    if (substr($afile['stored_filename'], 0, 5) == "data/")
	    {//файлы контента и системные

	        $relative_filename = substr($afile['stored_filename'], 5);
		    $full_path = $relative_filename;
		    $pathinfo = pathinfo($relative_filename);

            if(!$pathinfo['dirname'])//может быть и такое в архивах созданных вручную
                $kernel->pub_redirect_refresh("backup&backup=restore_step1&id=".$id."&total=".$total."&findex=".++$file_index);
		    if (!$kernel->pub_file_dir_create($pathinfo['dirname']))
		    {
    	        $kernel->pub_console_show("dir create failed: ".$pathinfo['dirname']);
    	        flush();
    	        return false;
		    }

		    $file_save_res = $kernel->pub_file_save($full_path, $file_content, $kernel->is_ftp_credentionals_set(), $need_santa_root_check);
            if (!$file_save_res)
            {
                $err = debug_backtrace();
                $err = $err[0];
                if (empty($err))
                    $err = "Failed to restore file ".$full_path;
                $this->error_last_set($err);
                $kernel->pub_console_show($err);
                flush();
            }
	    }
	    elseif (substr($afile['stored_filename'], 0, 4) == "sql/")
	    {//sql-файлы
	    	$queries = explode(";\n\r", $file_content);
            foreach ($queries AS $query)
            {
                $query = trim($query);
                if (empty($query))
                    continue;
                $query = str_replace("`%PREFIX%","`".PREFIX, $query);
                $kernel->runSQL($query);
            }
	    }
		$kernel->pub_redirect_refresh("backup&backup=restore_step1&id=".$id."&total=".$total."&findex=".++$file_index);
        return true;
    }

    function backup_download($id)
    {
        global $kernel;
        $full_path = false;
        $filename="";
        if (is_numeric($id) && $id > 0)
        {

            $data = $kernel->db_get_record_simple("_backup","id='".intval($id)."'");
            if ($data)
            {
                $real_filename = $data['real_filename'];
                $filename = $data['filename'];
                $full_path = $this->path_save.$real_filename;
            }
        }
        else
            $full_path = $this->path_save.$id;

        if (!$filename)
            $filename=$id;
        if ($full_path)
        {
            header("Cache-control: private");
            header("Content-Type: application/zip");
            header("Content-Size: ".filesize($full_path));
            header("Content-Disposition: attachment; filename=".$filename);
            ob_clean();
            flush();
            readfile($full_path);
            die();
        }
        else
            $kernel->pub_redirect_refresh("backup&backup=backup_files");
    }

    public function find_backups_by_prefix($prefix)
    {
        global $kernel;
        $backupFolder = $this->path_save;
        $ret = array();
        foreach($kernel->pub_files_list_get($backupFolder) as $bfile)
        {
            if((is_null($prefix) &&  strtolower(substr($bfile,-4)=='.zip')) || strpos($bfile,$prefix)===0)
                $ret[] = $bfile;
        }
        sort($ret);
        return $ret;
    }

    /**
     * Выполняет бэкап для правила
     * передаём для чего надо выполнять бэкап $needcontent, $needsystem, $needtables
     * т.к. при выполнении из админки может быть override для правила
     *
     * @param array $rule правило бэкапа
     * @param boolean $needcontent
     * @param boolean $needsystem
     * @param boolean $needtables
     * @param boolean $needdesign
     * @param string $descr описание
     * @return boolean
     */
    function backup_run_save($rule, $needcontent, $needsystem, $needtables, $needdesign, $descr='')
    {
        global $kernel;

        if (!is_dir($this->path_save))
            $kernel->pub_file_dir_create($this->path_save);

        $ignored_paths = $this->get_ignored_paths($rule['id']);
        $ignored_exts = $this->get_ignored_extensions($rule['id']);

        $data_files = array();
        $sql_files = array();
        $bool_content = "0";
        $bool_system = "0";
        $bool_mysql = "0";
        $bool_design = "0";
        $str_filename = array();

        if ($needcontent)
        {
            $content_files = $this->get_files4backup($this->path_content,$ignored_paths, $ignored_exts);
            $data_files = array_merge($content_files, $data_files);
            $str_filename[] = "content";
            $bool_content = "1";
        }
        if ($needsystem)
        {
            $system_files = $this->get_files4backup($this->path_system, $ignored_paths, $ignored_exts);
            $data_files = array_merge($system_files, $data_files);
            $str_filename[] = "system";
            $bool_system = "1";
        }
        if ($needdesign)
        {
            $system_files = $this->get_files4backup($this->path_design, $ignored_paths, $ignored_exts);
            $data_files = array_merge($system_files, $data_files);
            $str_filename[] = "design";
            $bool_design = "1";
        }

        if ($needtables)
        {
            $str_filename[] = "mysql";
            $bool_mysql = "1";
        }

        $delete_files = $copy_files = array();

        $uniq_id = $this->get_file_prefix4rule($rule).implode("-", $str_filename);
        if($rule['replace_every_days'] || $rule['replace_every_weeks'] || $rule['replace_every_months'] || $rule['replace_every_years'])
        {
            $fnPrefix = $uniq_id;
            if($rule['replace_every_days'])
            {
                $fileTypePrefix = $fnPrefix.'-days';
                $fn = $fileTypePrefix."-".date("Y-m-d").'.zip';
                $exTypeFiles = $this->find_backups_by_prefix($fileTypePrefix);
                $shouldCopy = false;
                if(!$exTypeFiles)
                    $shouldCopy = true;
                else {
                    $lastFn = end($exTypeFiles);
                    //находим дату у последнего файла
                    if(preg_match('~'.$fileTypePrefix.'-([\d]{4})-([\d]{2})-([\d]{2})~',$lastFn,$m)){
                        $lastFileDate = $m[1].'-'.$m[2].'-'.$m[3];
                        if(strtotime("-1 day") > strtotime($lastFileDate.' 00:00:00')) //если он был создан более дня назад, делаем новый
                        {
                            $shouldCopy = true;
                        }
                    }
                }
                if($shouldCopy)
                    $copy_files[] = $fn;

                if($shouldCopy && count($exTypeFiles)==$rule['replace_every_days'])
                {
                    $delFn = array_shift($exTypeFiles);
                    $delete_files[] = $delFn; //убираем первый
                }

            }
            if($rule['replace_every_weeks'])
            {
                $fileTypePrefix = $fnPrefix.'-weeks';
                $fn = $fileTypePrefix."-".date("Y-m-d").'.zip';
                $exTypeFiles = $this->find_backups_by_prefix($fileTypePrefix);
                $shouldCopy = false;
                if(!$exTypeFiles)
                    $shouldCopy = true;
                else {
                    $lastFn = end($exTypeFiles);
                    //находим дату у последнего файла
                    if(preg_match('~'.$fileTypePrefix.'-([\d]{4})-([\d]{2})-([\d]{2})~',$lastFn,$m)){//если он был создан неделю назад, делаем новый
                        $lastFileDate = $m[1].'-'.$m[2].'-'.$m[3];
                        if(strtotime("-1 week") > strtotime($lastFileDate.' 00:00:00'))
                        {
                            $shouldCopy = true;
                        }
                    }
                }
                if($shouldCopy)
                    $copy_files[] = $fn;

                if($shouldCopy && count($exTypeFiles)==$rule['replace_every_weeks'])
                {
                    $delFn = array_shift($exTypeFiles);
                    $delete_files[] = $delFn; //убираем первый
                }
            }
            if($rule['replace_every_months'])
            {
                $fileTypePrefix = $fnPrefix.'-months';
                $fn = $fileTypePrefix."-".date("Y-m-d").'.zip';
                $exTypeFiles = $this->find_backups_by_prefix($fileTypePrefix);
                $shouldCopy = false;
                if(!$exTypeFiles)
                    $shouldCopy = true;
                else {
                    $lastFn = end($exTypeFiles);
                    //находим дату у последнего файла
                    if(preg_match('~'.$fileTypePrefix.'-([\d]{4})-([\d]{2})-([\d]{2})~',$lastFn,$m)){//если он был создан месяц назад, делаем новый
                        $lastFileDate = $m[1].'-'.$m[2].'-'.$m[3];
                        if(strtotime("-1 month") > strtotime($lastFileDate.' 00:00:00'))
                        {
                            $shouldCopy = true;
                        }
                    }
                }
                if($shouldCopy)
                    $copy_files[] = $fn;

                if($shouldCopy && count($exTypeFiles)==$rule['replace_every_months'])
                {
                    $delFn = array_shift($exTypeFiles);
                    $delete_files[] = $delFn; //убираем первый
                }
            }
            if($rule['replace_every_years'])
            {
                $fileTypePrefix = $fnPrefix.'-years';
                $fn = $fileTypePrefix."-".date("Y-m-d").'.zip';
                $exTypeFiles = $this->find_backups_by_prefix($fileTypePrefix);
                $shouldCopy = false;
                if(!$exTypeFiles)
                    $shouldCopy = true;
                else {
                    $lastFn = end($exTypeFiles);
                    //находим дату у последнего файла
                    if(preg_match('~'.$fileTypePrefix.'-([\d]{4})-([\d]{2})-([\d]{2})~',$lastFn,$m)){
                        $lastFileDate = $m[1].'-'.$m[2].'-'.$m[3];
                        if(strtotime("-1 year") > strtotime($lastFileDate.' 00:00:00'))
                        {
                            $shouldCopy = true;
                        }
                    }
                }
                if($shouldCopy)
                    $copy_files[] = $fn;

                if($shouldCopy && count($exTypeFiles)==$rule['replace_every_years'])
                {
                    $delFn = array_shift($exTypeFiles);
                    $delete_files[] = $delFn; //убираем первый
                }
            }
            if(!$copy_files){
                $this->error_last_set('no files to copy');
                return false;
            }
        }
        if ($needtables)
        {
            $kernel->pub_file_dir_create($this->path_save.$uniq_id."/");
            $sql_files = $this->dump_mysql_table4backup($rule['id'], $uniq_id);
            if (!$sql_files){
                $this->error_last_set('no sql files');
                return false;
            }
        }

        if (!$data_files && !$sql_files){
            $this->error_last_set('no sql and data files');
            return false;
        }
        $description = htmlspecialchars(strip_tags($descr));

        define('PCLZIP_TEMPORARY_DIR', $this->path_save);
		include(dirname(dirname(__FILE__))."/components/pclzip/pclzip.lib.php");
		$real_archive_name = $uniq_id."-".date("Y-m-d")."-".substr(md5(time()), 0, 8).".zip";
		$name_new_archive = $this->path_save.$real_archive_name;

        if(file_exists($name_new_archive))
            $kernel->pub_file_delete($name_new_archive);

        $archive = new PclZip($name_new_archive);
        if (empty($sql_files))
        {//только системные и контент-файлы
            $archive->create($data_files, PCLZIP_OPT_REMOVE_PATH, $this->root, PCLZIP_OPT_ADD_PATH, "data");
        }
        elseif (empty($data_files))
        {//только SQL-дампы
            $archive->create($sql_files, PCLZIP_OPT_REMOVE_PATH, $this->path_save.$uniq_id, PCLZIP_OPT_ADD_PATH, "sql");
            $kernel->pub_file_delete($this->path_save.$uniq_id."/", true);
        }
        else
        {// дампы + файлы
            $archive->create($data_files, PCLZIP_OPT_REMOVE_PATH, $this->root, PCLZIP_OPT_ADD_PATH, "data");
            $archive->add($sql_files, PCLZIP_OPT_REMOVE_PATH, $this->path_save.$uniq_id."/", PCLZIP_OPT_ADD_PATH, "sql");
            $kernel->pub_file_delete($this->path_save.$uniq_id."/", true);
        }
        $archive_filename = date("Y-m-d")."-".implode("-", $str_filename).".zip";


        $real_arch_names = $copy_files;
        foreach($real_arch_names as $real_archive_name)
        {
            $sql = "INSERT INTO
                    ".PREFIX."_backup
                    (real_filename, filename, `date`, content, system, mysql, design, description)
                    VALUES
                    (
                    '".$real_archive_name."',
                    '".$archive_filename."',
                    NOW(),
                    '".$bool_content."',
                    '".$bool_system."',
                    '".$bool_mysql."',
                    '".$bool_design."',
                    '".$description."'
                    )
                    ";
            $kernel->runSQL($sql);
        }
        //аплоад на фтп, если указаны данные
        if (!empty($rule['ftphost']) && !empty($rule['ftpuser']) && !empty($rule['ftppass']) && !empty($rule['ftpdir']))
        {
            require_once(dirname(dirname(__FILE__))."/include/ftpshnik.class.php");
            $ftpshnik = new ftpshnik($rule['ftphost'], $rule['ftpuser'], $rule['ftppass'], $rule['ftpdir']);
            if (!$ftpshnik->init($rule['ftpdir']))
            {
                 $this->error_last_set("FTP: ".$ftpshnik->getLastError());
                 return false;
            }
            $ftpshnik->deleteFile($archive_filename);
            foreach($copy_files as $copyFile)
            {
                $ftpshnik->putFileEx($name_new_archive,$copyFile);
            }
            foreach($delete_files as $delFile)
            {
                $ftpshnik->deleteFile($delFile);
            }
            $ftpshnik->disconnect();
        }

        if($copy_files){
            foreach($copy_files as $copyFile){
                $kernel->pub_file_copy($name_new_archive,$this->path_save.$copyFile);

            }
            //если копировали оригинальный в другие, удаляем его
            $kernel->pub_file_delete($name_new_archive);
        }
        foreach($delete_files as $delFile)
        {
            $kernel->runSQL("DELETE FROM ".PREFIX."_backup WHERE `real_filename`= '".$kernel->pub_str_prepare_set($delFile)."'");
            $kernel->pub_file_delete($this->path_save.$delFile);
        }

        return true;
    }

    function backup_delete($id)
    {
        global $kernel;
        $rec = $kernel->db_get_record_simple("_backup","id=".intval($id));
        if(!$rec)
            return false;
        foreach($this->find_backups_by_prefix('backup'.$rec['id']."-") as $bfile)
        {
            $kernel->pub_file_delete($this->path_save.$bfile);
        }
        $sql = "DELETE FROM ".PREFIX."_backup WHERE id='".$id."'";
        $kernel->runSQL($sql);
        return true;
    }


    function backup_table_get()
    {
        global $kernel;
        $html = "";
        $sql = "SELECT
                *, UNIX_TIMESTAMP(date) as `mtime`
                FROM
                ".PREFIX."_backup
                ORDER BY
                date
                DESC
                ";
        $items = $kernel->db_get_list($sql);


		$sum = count($items);
        if ($sum > 0)
        {
            $lines = "";
            $template = $kernel->pub_template_parse("admin/templates/default/backup_files.html");
            $html = $template['backup_table_made'];
            $i = 1;
            foreach($items as $data)
            {
                $archive_content_arr = array();
                $line = $template['backup_table_row'];
                $line = str_replace("[#tr_class#]", $kernel->pub_table_tr_class($i), $line);
                if ($data['content'] == "1")
                    $archive_content_arr[] = "[#admin_save_components_save_content#]";
                if ($data['system'] == "1")
                    $archive_content_arr[] = "[#admin_save_components_save_system#]";
                if ($data['mysql'] == "1")
                    $archive_content_arr[] = "[#admin_save_components_save_mysql#]";
                if ($data['design'] == "1")
                    $archive_content_arr[] = "[#admin_save_components_save_design#]";
                $archive_content_str = implode(", ", $archive_content_arr);

                $data ['real_filename'] = $kernel->pub_redirect_for_form("rules&backup=dwld&id=".trim($data['id']));
                $data['date'] = $kernel->pub_data_to_string(date('Y.m.d', $data['mtime']), true);
                $line = str_replace("%archive_content%", $archive_content_str, $line);

                $line = $kernel->pub_array_key_2_value($line, $data);
                $line = str_replace("%num_order%", $i, $line);
                $lines .= $line;
                $i++;
            }
            $html = str_replace("%lines%", $lines, $html);
            $html = str_replace("%action%", $kernel->pub_redirect_for_form("backup&backup=delete_selected_made"),$html);
        }
        return $html;
    }

    function backup_table_get_uploaded($files)
    {
        global $kernel;


        $result = $kernel->db_get_list_simple('_backup', true);

        $array = array();

		foreach($result as $row)
        {
            $array[] = $row['real_filename'];
        }
        $files = array_diff($files, $array);

        $template = $kernel->pub_template_parse("admin/templates/default/backup_files.html");
        $content = $template['backup_table_uploaded'];

        $lines = ''; $i = 1;
        foreach ($files as $value)
        {
            if ($value == '.' || $value == '..'|| $value == '.htaccess') {
            	continue;
            }
            $line = $template['backup_table_row'];
            $line = str_replace("[#tr_class#]", $kernel->pub_table_tr_class($i), $line);

            $info = stat('backup/'.($kernel->is_windows?iconv('utf-8','windows-1251',$value):$value));

            $line = str_replace("%num_order%", $i++, $line);
            $line = str_replace("%archive_content%", 'Неизвестно', $line);
            $line = str_replace("%description%", '', $line);
            $line = str_replace('%real_filename%', $kernel->pub_redirect_for_form("rules&backup=dwld&id=".$value), $line);
            $line = str_replace(array('%id%', '%filename%'),  htmlspecialchars($value), $line);
            $line = str_replace("%date%", $kernel->pub_data_to_string(date('Y.m.d', $info['mtime']), true), $line);
            $lines .= $line;
        }
        $content = str_replace("%lines%", $lines, $content);
        $content = str_replace("%action%", $kernel->pub_redirect_for_form("backup&backup=delete_selected_uploaded"), $content);

        return $content;
    }

    /**
     * Возвращает список всех вложенных папок в папке (рекурсивно)
     *
     * @param string $path путь к папке
     * @param boolean $recursive рекурсивно?
     * @return array
     */
    function dirlist_recursive_get($path, $recursive=true)
    {
        $root = $this->root;
        $dir = opendir($root.$path);
        $array = array();
        while ($file = readdir($dir))
        {
            if (($file != ".") && ($file != ".."))
            {
                if (is_dir($root.$path."/".$file."/"))
                {
                    //$array[] = $root.$path."/".$file."/";
                    $array[] = $path."/".$file."/";
                    if ($recursive)
                        $array = array_merge($array, $this->dirlist_recursive_get($path."/".$file, $recursive));
                }
            }
        }
        closedir($dir);
        return $array;
    }

    /**
     * Возвращает список всех вложенных файлов в папке (рекурсивно)
     *
     * @param string $path путь к папке
     * @param boolean $recursive рекурсивно?
     * @return array
     */
    function filelist_recursive_get($path, $recursive=true)
    {
        $root = $this->root;
        $dir = opendir($root.$path);
        $array = array();
        while ($file = readdir($dir))
        {
            if (($file != ".") && ($file != ".."))
            {
                if (is_file($root.$path."/".$file))
                {
                    $array[] = $root.$path."/".$file;
                }
                elseif (is_dir($root.$path."/".$file) && $recursive)
                {
                    $array = array_merge($array, $this->filelist_recursive_get($path."/".$file, $recursive));
                }
            }
        }
        closedir($dir);
        return $array;
    }

    /**
     * Возвращает файлы по указанным путям, которые надо забэкапить
     * с учётом игнорируемых путей и расширений
     *
     * @param array $paths пути, откуда собираем файлы
     * @param array $ignored_paths игнорируемые пути
     * @param array $ignored_exts игнорируемые расширения
     * @return array
     */
    function get_files4backup($paths, $ignored_paths, $ignored_exts)
    {
        $root = $this->root;
        $saved_files = array();
        if (count($paths) ==1 && $paths[0] == "/design")
        {//если это папка design, то добавим и юзер-темплейты всех модулей
            $mod_dirs = $this->dirlist_recursive_get("/modules", false);
            foreach ($mod_dirs as $mod_dir)
            {
                if (is_dir($root.$mod_dir."templates_user"))
                    $paths[] = $mod_dir."templates_user";
            }
        }
        foreach ($paths as $path)
        {
            $array_files = $this->filelist_recursive_get($path);
            foreach ($array_files as $filepath)
            {
                $epos=strrpos($filepath, ".");
                if ($epos === false)
                    continue;
                $ext = strtolower(substr($filepath, $epos+1));
                if (in_array($ext, $ignored_exts))
                    continue;
                foreach ($ignored_paths as $ipath)
                {
                    if (strpos($filepath, $ipath) !== false)
                        continue 2;
                }
                $saved_files[] = $filepath;
            }
        }
        return $saved_files;
    }

    /**
     * Возвращает список таблиц, которые на надо бэкапить
     *
     * @param number $id ID-шник правила
     * @return array
     */
    private function get_ignored_tables($id)
    {
        global $kernel;
		$rows = $kernel->db_get_list_simple('_backup_ignoredtables', array('ruleid'=> $id));

		foreach($rows as $row)
		{
			$items[] = $row['tablename'];
		}

        return $items;
    }

    /**
     * Возвращает список расширений, которые на надо бэкапить
     *
     * @param number $id ID-шник правила
     * @return array
     */
    private function get_ignored_extensions($id)
    {
		global $kernel;

		$rows = $kernel->db_get_list_simple('_backup_ignoredexts', array('ruleid'=> $id));

		foreach($rows as $row)
		{
			$items[] = $row['ext'];
		}
		return $items;
    }
    /**
     * Возвращает список путей, которые на надо бэкапить
     *
     * @param number $id ID-шник правила
     * @return array
     */
    private function get_ignored_paths($id)
    {
        global $kernel;

		$rows = $kernel->db_get_list_simple('_backup_ignoredpaths', array('ruleid'=> $id));
        $items = array();

		foreach($rows as $row)
		{
			$items[] =$row['path'];
		}


        return $items;
    }



    /**
     * Сохраняет всю структуру mySql в файл для бэакапа в файлы
     * (за исключением игнорируемых таблиц)
     *
     * @param number $ruleid ID-шник правила
     * @param string $uniqID уникальный ID-шник бэкапа
     * @return array
     */
    function dump_mysql_table4backup($ruleid, $uniqID)
    {
    	global $kernel;
    	$ignored_tables = $this->get_ignored_tables($ruleid);
    	$tables = $kernel->db_get_tables();
    	$saved_files = array();
		foreach ($tables as $table_name)
		{

       		//если таблица в списке игнорируемых, то пропускаем
       		if (in_array($table_name,$ignored_tables))
       		    continue;
            $sql_file_path = $this->path_save.$uniqID."/".$table_name.".sql";
       		$table_name_prefixed = preg_replace("/^".PREFIX."_/", "%PREFIX%_",$table_name);
       		//Значит это та таблица, которую нам надо загужать
       		//Сначала пропишем строку с удаленнием этой таблицы
            $this->put_line2sqlFile("DROP TABLE IF EXISTS `".$table_name_prefixed."`",$sql_file_path);

			$row_table = $kernel->db_get_list_one("SHOW CREATE TABLE `".$table_name."`");
			$table_sql = $row_table[1];
			$table_sql = str_replace($table_name, $table_name_prefixed, $table_sql);
            $this->put_line2sqlFile($table_sql, $sql_file_path);

			//Теперь можем вставлять непосредственно данные

			$res = $kernel->db_get_list_simple($table_name, true);
			foreach($res as $data)
			{
				foreach ($data as $key => $val)
				{
                    if (is_null($val))
						$data[$key] = "NULL";
					else
						$data[$key] = "'".$kernel->pub_str_prepare_set($val)."'";
				}
				$this->put_line2sqlFile("INSERT INTO `".$table_name_prefixed."` VALUES (".join(",",$data).")",$sql_file_path);
			}
			$saved_files[] = $sql_file_path;
			//if (!$kernel->pub_file_save($sql_file_path, $str_save)) return false;
   		}
   		return $saved_files;
    }

    function put_line2sqlFile($line,$file){
        file_put_contents($file,$line.";\n\r",FILE_APPEND);
    }


    /**
     * Возвращает список бэкап-правил из БД
     *
     * @return array
     */
    function get_backup_rules()
    {
        global $kernel;

		$items = $kernel->db_get_list_simple('_backup_rules', true);

		return $items;
	}

    /**
     * Возвращает бэкап-правило по ID
     *
     * @param number $id
     * @return array
     */
    function get_backup_rule($id)
    {
        global $kernel;

		$items = $kernel->db_get_list_simple('_backup_rules', array('id'=>$id), '*', '1');

		if (!empty($items))
		{
			$return = 0;
		}
		else
		{
			$return = $items[0];
		}

        return $return;
    }

    /**
     * Возвращает бэкап-правило по ID
     *
     * @param string $stringid
     * @return array
     */
    function get_backup_rule_by_stringid($stringid)
    {
        global $kernel;
        $query = "SELECT * FROM `".PREFIX."_backup_rules` WHERE `stringid`='".$kernel->pub_str_prepare_set($stringid)."' LIMIT 1";
        $result = $kernel->runSQL($query);
        $return = false;
        if ($row = mysqli_fetch_assoc($result))
            $return = $row;
        mysqli_free_result($result);
        return $return;
    }

    /**
     * Админ интерфейс
     *
     * @param string $backup_action
     * @param array $my_get
     * @param array $my_post
     * @return string
     */
    function backup_start($backup_action, $my_get, $my_post)
    {
        global $kernel;
        $html_content='';
        switch ($backup_action)
		{
            case 'delete_selected_made':
                if(isset($_POST['bf']) && is_array($_POST['bf']))
                {
                    $ids = array_keys($_POST['bf']);
                    foreach($ids as $id)
                    {
                        $this->backup_delete($id);
                    }
                }
                return $kernel->pub_httppost_response("[#common_selected_has_been_deleted#]","backup&backup=backup_files");
            case 'delete_selected_uploaded':
                if(isset($_POST['bf']) && is_array($_POST['bf']))
                {
                    $files = array_keys($_POST['bf']);
                    $bfiles = $this->find_backups_by_prefix(null);
                    foreach($files as $file)
                    {
                        $this->delete_uploaded_file($file,$bfiles);
                    }
                }
                return $kernel->pub_httppost_response("[#common_selected_has_been_deleted#]","backup&backup=backup_files");
		    default:
		    case 'rules':
		        $template = $kernel->pub_template_parse($this->root."/admin/templates/default/backup_rules.html");
                $html_content = $template['backup_rules_table'];
		        $rules = $this->get_backup_rules();
		        $lines = '';
		        $i=1;
		        foreach ($rules as $rule)
		        {
		            $line = $template['backup_rules_row'];
		            $line = str_replace("[#tr_class#]", $kernel->pub_table_tr_class($i), $line);
		            $line = $kernel->pub_array_key_2_value($line, $rule);
		            $line = str_replace("%num_order%", $i++, $line);
		            $lines .= $line;
		        }
		        $html_content = str_replace("%lines%", $lines, $html_content);
		        return $html_content;
		        break;
		    case 'edit_rule':
		        $id = $my_get['id'];

		        $template = $kernel->pub_template_parse($this->root."/admin/templates/default/backup_rules.html");
		        $html_content = $template['backup_rule_edit'];
		        $html_content = str_replace("%form_action%",$kernel->pub_redirect_for_form("backup&backup=save_rule&id=".$id), $html_content);

                if ($id==0)
                {
                    $rule = array("needcontent"=>0, "needsystem"=>0, "needtables"=>0, "needdesign"=>0, "name"=>"", "stringid"=>"",
                                  'replace_every_days'=>1,  'replace_every_weeks'=>'', 'replace_every_months'=>'', 'replace_every_years'=>'',
                                  "ftphost"=>"","ftpuser"=>"", "ftppass"=>"", "ftpdir"=>"/","id"=>0);
                    $edit_details_link='';
                }
                else
                {
                    $rule = $this->get_backup_rule($id);
                    $edit_details_link=$template['rule_details_link'];
                }
                $html_content=str_replace('%rule_details_link%',$edit_details_link,$html_content);
                $html_content = str_replace("%name%", htmlspecialchars($rule['name']), $html_content);

		        if ($rule['needcontent']==1)
		            $html_content = str_replace("%needcontent%", "checked", $html_content);
		        else
		            $html_content = str_replace("%needcontent%", "", $html_content);
		        if ($rule['needsystem']==1)
		            $html_content = str_replace("%needsystem%", "checked", $html_content);
		        else
		            $html_content = str_replace("%needsystem%", "", $html_content);
		        if ($rule['needtables']==1)
		            $html_content = str_replace("%needtables%", "checked", $html_content);
		        else
		            $html_content = str_replace("%needtables%", "", $html_content);
		        if ($rule['needdesign']==1)
		            $html_content = str_replace("%needdesign%", "checked", $html_content);
		        else
		            $html_content = str_replace("%needdesign%", "", $html_content);
		        $html_content = $kernel->pub_array_key_2_value($html_content, $rule);
		        return $html_content;
		        break;
		    case 'save_rule':
		        $id = intval($my_post['id']);
		        $stringid = $kernel->pub_httppost_get('stringid', false);
		        $name = $kernel->pub_httppost_get('name', false);

		        if (empty($name))
		             return $kernel->pub_httppost_response("[#admin_backup_error_empty_rule_name#]");
		        if (empty($stringid))
		            $stringid = $this->translate2stringid($name);
		        else
		            $stringid = $this->translate2stringid($stringid);
		        if (empty($stringid))
		            return $kernel->pub_httppost_response("[#admin_backup_error_incorrect_rule_stringid#]");

		        $stringid = strtolower($stringid);
		        $ex_rule = $this->get_backup_rule_by_stringid($stringid);
		        if ($ex_rule && $ex_rule['id']!=$id)
		            return $kernel->pub_httppost_response("[#admin_backup_error_existing_rule_stringid#]");

		        $name = $kernel->pub_str_prepare_set($name);
		        $stringid = $kernel->pub_str_prepare_set($stringid);
		        if (empty($my_post['needcontent']))
		            $needcontent = 0;
		        else
		            $needcontent = 1;
		        if (empty($my_post['needsystem']))
		            $needsystem = 0;
		        else
		            $needsystem = 1;
		        if (empty($my_post['needtables']))
		            $needtables = 0;
		        else
		            $needtables = 1;
		        if (empty($my_post['needdesign']))
		            $needdesign = 0;
		        else
		            $needdesign = 1;

                $every_days = intval($kernel->pub_httppost_get('replace_every_days'));
                if($every_days<1)
                    $every_days = "0";

                $every_weeks = intval($kernel->pub_httppost_get('replace_every_weeks'));
                if($every_weeks<1)
                    $every_weeks = "0";

                $every_months = intval($kernel->pub_httppost_get('replace_every_months'));
                if($every_months<1)
                    $every_months = "0";

                $every_years = intval($kernel->pub_httppost_get('replace_every_years'));
                if($every_years<1)
                    $every_years = "0";

		        if ($id == 0)
		            $query = "INSERT INTO `".PREFIX."_backup_rules`
		                    (`stringid`,`name`,`ftphost`, `ftpuser`,`ftppass`, `ftpdir`, `needcontent`, `needsystem`, `needtables`, `needdesign`,
		                    `replace_every_days`,`replace_every_weeks`,`replace_every_months`,`replace_every_years`)
		            		 VALUES
		            		 ('".$stringid."','".$name."', '".$my_post['ftphost']."', '".$my_post['ftpuser']."','".$my_post['ftppass']."','".$my_post['ftpdir']."', ".$needcontent.", ".$needsystem.", ".$needtables.", ".$needdesign.",
		            		 ".$every_days.",".$every_weeks.",".$every_months.",".$every_years.")";
		        else
		            $query ="UPDATE `".PREFIX."_backup_rules` SET `stringid`='".$stringid."',
		                     `name`='".$name."',
		            		 `ftphost`='".$my_post['ftphost']."',
		            		 `ftpuser`='".$my_post['ftpuser']."',
		            		 `ftppass`='".$my_post['ftppass']."',
		            		 `ftpdir`='".$my_post['ftpdir']."',
		            		 `needcontent`=".$needcontent.",
		            		 `needsystem`=".$needsystem.",
		            		 `needtables`=".$needtables.",
		            		 `needdesign`=".$needdesign.",
		            		 `replace_every_days`=".$every_days.",
		            		 `replace_every_weeks`=".$every_weeks.",
		            		 `replace_every_months`=".$every_months.",
		            		 `replace_every_years`=".$every_years."
		            		WHERE `id`=".$id;
		        $kernel->runSQL($query);
		        return $kernel->pub_httppost_response("[#admin_backup_rule_saved_msg#]","backup&backup=rules");
		    case 'delete_rule':
		        $id = $my_get['id'];
		        $query = "DELETE FROM `".PREFIX."_backup_rules` WHERE `id`=".$id;
		        $kernel->runSQL($query);
		        $query = "DELETE FROM `".PREFIX."_backup_ignoredexts` WHERE `ruleid`=".$id;
		        $kernel->runSQL($query);
		        $query = "DELETE FROM `".PREFIX."_backup_ignoredpaths` WHERE `ruleid`=".$id;
		        $kernel->runSQL($query);
		        $query = "DELETE FROM `".PREFIX."_backup_ignoredtables` WHERE `ruleid`=".$id;
		        $kernel->runSQL($query);
		        //return $kernel->pub_httppost_response("[#admin_backup_rule_deleted_msg#]","backup&backup=rules");
		        $kernel->pub_redirect_refresh("backup&backup=rules");
		        break;
		    case 'edit_rule_details':
		        $ruleid = $my_get['id'];
		        $rule = $this->get_backup_rule($ruleid);
		        $template = $kernel->pub_template_parse($this->root."/admin/templates/default/backup_rule_details.html");
                $html_content = $template['main'];
                $html_content = str_replace("%form_action%",$kernel->pub_redirect_for_form("backup&backup=run_save"), $html_content);
                $html_content = str_replace("%name%",$rule['name'], $html_content);

                if ($rule['needcontent']==1)
                    $html_content = str_replace("%needcontent_checked%", "checked", $html_content);
                else
                    $html_content = str_replace("%needcontent_checked%", "", $html_content);
                if ($rule['needsystem']==1)
                    $html_content = str_replace("%needsystem_checked%", "checked", $html_content);
                else
                    $html_content = str_replace("%needsystem_checked%", "", $html_content);
                if ($rule['needtables']==1)
                    $html_content = str_replace("%needtables_checked%", "checked", $html_content);
                else
                    $html_content = str_replace("%needtables_checked%", "", $html_content);
                if ($rule['needdesign']==1)
                    $html_content = str_replace("%needdesign_checked%", "checked", $html_content);
                else
                    $html_content = str_replace("%needdesign_checked%", "", $html_content);

				//игнорируемые расширения
				$block = $template['ignored_ext_table'];
				$ignored_exts = $this->get_ignored_extensions($ruleid);
			    $i = 1;
				$lines = '';
				foreach ($ignored_exts as $ext)
				{
				    $line = $template['ignored_ext_table_row'];
				    $line = str_replace("[#tr_class#]", $kernel->pub_table_tr_class($i), $line);
				    $line = str_replace("%num_order%", $i++, $line);
				    $line = str_replace("%ext%", $ext, $line);
				    $line = str_replace("%ext_escaped%", urlencode($ext), $line);
				    $lines .= $line;
				}
				$block = str_replace("%lines%", $lines, $block);
				$block = str_replace("%form_action_add%", $kernel->pub_redirect_for_form("backup&backup=add_ext"), $block);
				$html_content = str_replace("%ignored_exts%", $block, $html_content);

				//игнорируемые пути
				$block = $template['ignored_paths_table'];
				$ignored_paths = $this->get_ignored_paths($ruleid);
				//print_r($ignored_exts);
			    $i = 1;
				$lines = '';
				foreach ($ignored_paths as $path)
				{
				    $line = $template['ignored_paths_table_row'];
				    $line = str_replace("[#tr_class#]", $kernel->pub_table_tr_class($i), $line);
				    $line = str_replace("%num_order%", $i++, $line);
				    $line = str_replace("%path%", $path, $line);
				    $line = str_replace("%path_escaped%", urlencode($path), $line);
				    $lines .= $line;
				}
				$block = str_replace("%lines%", $lines, $block);
				$block = str_replace("%form_action_add%", $kernel->pub_redirect_for_form("backup&backup=add_path"), $block);
				$html_content = str_replace("%ignored_paths%", $block, $html_content);

				//игнорируемые таблицы
				$block = $template['ignored_table'];
				$ignored_tables = $this->get_ignored_tables($ruleid);
				$i = 1;
				$lines = '';
				foreach ($ignored_tables as $itable)
				{
				    $line = $template['ignored_table_row'];
				    $line = str_replace("[#tr_class#]", $kernel->pub_table_tr_class($i), $line);
				    $line = str_replace("%num_order%", $i++, $line);
				    $line = str_replace("%tablename%", $itable, $line);
				    $lines .= $line;
				}

			    $select_add = '';
			    $all_tables = $kernel->db_get_tables();
				foreach ($all_tables as $table)
				{
				    if (in_array($table, $ignored_tables))
				        continue;
				    $line_add = $template['ignored_tables_add_option'];
				    $line_add = str_replace("%tablename%", $table, $line_add);
				    $select_add .= $line_add;
				}
				$block = str_replace("%lines%", $lines, $block);
				$block = str_replace("%form_action_add%", $kernel->pub_redirect_for_form("backup&backup=add_it"), $block);
				$block = str_replace("%select_options%", $select_add, $block);
				$html_content = str_replace("%ignored_tables%", $block, $html_content);
				$html_content = str_replace("%ruleid%", $ruleid, $html_content);
		        break;
		    case 'backup_files':
				//Копирование вновь загруженного файла, если он есть
				if (isset($_FILES['backup_file']))
				{
				    if (is_uploaded_file($_FILES['backup_file']['tmp_name']))
				    {
				        $moveto = $this->path_save.$_FILES['backup_file']['name'];
				        move_uploaded_file($_FILES['backup_file']['tmp_name'], $moveto);
				    }
				    $kernel->pub_redirect_refresh_reload('backup&backup=backup_files');
				}

				$upload_max_filesize = ini_get('upload_max_filesize');

				ini_set('upload_max_filesize','20M');

		        $template = $kernel->pub_template_parse($this->root."/admin/templates/default/backup_files.html");
                $html_content = $template['main'];
		        $block = $this->backup_table_get();
		        $block = str_replace("%header%", $template['backup_made_header'], $block);
				$html_content = str_replace("%backup_table%", $block, $html_content);
				$html_content = str_replace("%upload_max_filesize%", $upload_max_filesize, $html_content);
				$html_content = str_replace("%form_action%", $kernel->pub_redirect_for_form("backup&backup=backup_files"), $html_content);
				$files = $this->find_backups_by_prefix(null);
				$block = $this->backup_table_get_uploaded($files);
				$block = str_replace("%header%", $template['backup_uploaded_header'], $block);
				$html_content = str_replace("%uploaded_files%", $block, $html_content);
				return $html_content;
		    case 'run_save':
                set_time_limit(300);
		        $ruleid = $my_post['ruleid'];
		        $rule = $this->get_backup_rule($ruleid);
		        $backup_res = $this->backup_run_save($rule, $my_post['needcontent']==1,
		                                $my_post['needsystem']==1,
		                                $my_post['needtables']==1,
		                                $my_post['needdesign']==1,
		                                $my_post['description']);
		        if ($backup_res)
		            return $kernel->pub_httppost_response("[#admin_backup_rule_finished_ok_msg#]","/admin",0);
		        else
		            return $kernel->pub_httppost_errore($this->error_last_get(),true);

		        break;

		    //удаление бэкапа
		    case 'delete':
                $id = $kernel->pub_httpget_get('id');
                if (is_numeric($id))
                    $this->backup_delete($id);
                else
                    $this->delete_uploaded_file($id);
        		$kernel->pub_redirect_refresh("backup&backup=backup_files");
		        break;

		    case 'dwld':
		        $id_dwld = $kernel->pub_httpget_get('id');
        		$this->backup_download($id_dwld);
		        break;

		    case 'restore_step1':
		        $id_restore = $my_get['id'];
		        if (isset($my_get['findex']))
		            $file_index = $my_get['findex'];
		        else
		            $file_index = 0;
		        $this->backup_restore($id_restore, $file_index);
                $kernel->pub_redirect_refresh("backup&backup=backup_files");
                break;
		    case 'delete_it':
		        $ruleid = $my_get['ruleid'];
		        $table_name = $my_get['tn'];
		        $kernel->runSQL("DELETE FROM `".PREFIX."_backup_ignoredtables` WHERE `tablename`='".$table_name."' AND `ruleid`=".$ruleid);
		        $kernel->pub_redirect_refresh('backup&backup=edit_rule_details&id='.$ruleid);
		        break;
		    case 'add_it':
		        $table_names = array_keys($my_post['tn']);
		        $ruleid = $my_post['ruleid'];
		        foreach ($table_names as $tn)
		        {
		            $kernel->runSQL("INSERT INTO `".PREFIX."_backup_ignoredtables` (`tablename`, `ruleid`) VALUES ('".$tn."', ".$ruleid.")");
		        }
		        $kernel->pub_redirect_refresh_reload('backup&backup=edit_rule_details&id='.$ruleid);
		        break;

		    case 'delete_ignored_path':
		        $ruleid = $my_get['ruleid'];
		        $name = $kernel->pub_str_prepare_set(urldecode($my_get['path']));
		        $query = "DELETE FROM `".PREFIX."_backup_ignoredpaths` WHERE `path`='".$name."' AND `ruleid`=".$ruleid;
		        $kernel->runSQL($query);
		        $kernel->pub_redirect_refresh('backup&backup=edit_rule_details&id='.$ruleid);
		        break;
		    case 'add_path':
		        $path = $my_post['path'];
		        $ruleid = $my_post['ruleid'];
		        $kernel->runSQL("INSERT INTO `".PREFIX."_backup_ignoredpaths` (`path`, `ruleid`) VALUES ('".$path."', ".$ruleid.")");
		        $kernel->pub_redirect_refresh_reload('backup&backup=edit_rule_details&id='.$ruleid);
		        break;

		    case 'delete_ext':
		        $ruleid = $my_get['ruleid'];
		        $name = $kernel->pub_str_prepare_set(urldecode($my_get['ext']));
		        $query = "DELETE FROM `".PREFIX."_backup_ignoredexts` WHERE `ext`='".$name."' AND `ruleid`=".$ruleid;
		        $kernel->runSQL($query);
		        $kernel->pub_redirect_refresh('backup&backup=edit_rule_details&id='.$ruleid);
		        break;
		    case 'add_ext':
		        $ruleid = $my_post['ruleid'];
		        $name = strtolower(trim($kernel->pub_str_prepare_set($my_post['ext'])));
		        $kernel->runSQL("INSERT INTO `".PREFIX."_backup_ignoredexts` (`ext`,`ruleid`) VALUES ('".$name."',".$ruleid.")");
		        $kernel->pub_redirect_refresh_reload('backup&backup=edit_rule_details&id='.$ruleid);
		        break;

		}
	   return $html_content;
    }


    /**
     * Конвертирует строку в приемлимую для использования в качестве строкового идентификатора
     *
     * @param string $s строка для конвертирования
     * @return string
     */
    private function translate2stringid($s)
    {
        global $kernel;
        $s = $kernel->pub_translit_string($s);
        $s = preg_replace("/[^0-9a-z_]/i", '', $s);
        return $s;
    }


    private function delete_uploaded_file($fname,$bfiles = null)
    {
        global $kernel;
        if(is_null($bfiles))
            $bfiles = $this->find_backups_by_prefix(null);
        if(in_array($fname,$bfiles))
            $kernel->pub_file_delete('backup/'.$fname);
    }
}
