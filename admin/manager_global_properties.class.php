<?php
/**
 * Обеспечевает редактирование общих настроек Всего движка
 *
 */
class manager_global_properties
{
	var $curent_action = '';
	var $manager_update_delimiter;

	function manager_global_properties()
	{

		if (isset($_SESSION['vars_kernel']['curent_action_in_globalprop']))
			$this->curent_action = $_SESSION['vars_kernel']['curent_action_in_globalprop'];
		else
			$this->curent_action = '';

		//Определим разделитель для файлов, при этом не в явном виде
		//что бы по нему не разделился этот файл во время передачи по обдейту
        $bad_delimiter = "92e67e11a16b91---553d74dcb1f760eb76";
        $delimiter = str_replace("---", "", $bad_delimiter);
        $this->manager_update_delimiter = $delimiter;
	}

    /**
     * Предопределённая функция для формирования меню
     *
     * Функция орпеделяет какие элементы меню присутсвуют в меню раздела
     * @param pub_interface $show Объект класса pub_interface
     * @return void
     * @access private
     */
	function interface_get_menu($show)
    {
        $show->set_menu_block('[#global_prop_label_menu1#]');
        $show->set_menu("[#global_prop_label_save_rezerv#]","backup");
        $show->set_menu("[#global_prop_label_backup_files#]","backup&backup=backup_files");
        $show->set_menu("[#global_prop_label_constant#]","sys_prop");
        $show->set_menu("Настройки безопасности","sec_prop");
		$show->set_menu("Поля в структуре","page_fields");
        $show->set_menu("[#global_prop_label_info_site#]","info_site");
        //$show->set_menu_block('[#global_prop_label_sys_action#]');
        $show->set_menu("[#global_prop_label_sys_action#]","glob_action");
        $show->set_menu_default('form_save');
    }

	function start()
    {
    	global $kernel;
        $my_post = $kernel->pub_httppost_get();
        $my_get = $kernel->pub_httpget_get();

        $action = $kernel->pub_section_leftmenu_get();
		$html_content = '';
        switch ($action)
        {
            //Формирует форму для резервного копирования сайта
            default:
            case 'backup':
                $backup_action = $kernel->pub_httpget_get("backup");
				$backup = new backup();
				$html_content .= $backup->backup_start($backup_action, $my_get, $my_post);
            	break;

            //Выводим форму, с глобальными параметрами, прописанными та же в файле ini.php
            case 'sys_prop':

            	$html_content = $this->show_form_global_param();
                break;

            case 'sec_prop':
                    $html_content = $this->show_form_sec_param();
            break;
            case 'save_sec_prop':
                    $html_content = $this->save_sec_prop();
            break;

            case 'get_sec_log':
                $this->get_sec_log();
            break;

            case 'del_sec_log':
                $this->del_sec_log();
            break;

            case 'delete_ip':
                $html_content = $this->delete_ip();
            break;


			case 'delete_allow':
				$html_content = $this->delete_allow();
				break;

            //Записывает отредактированные свойства в ini файл
            case 'save_sys_prop':
                $this->save_sys_properties();
                //$kernel->pub_redirect_refresh_reload("sys_prop");
                $html_content = $kernel->pub_json_encode(array("info"=>"[#kernel_ajax_data_saved_ok#]","success"=>true));
            break;

            //Выводит информацию о ПО используемом на сайте
            case 'info_site':
            	$html_content = $this->show_form_info();
			break;

			//Выводит список доступных системных дейсвтий, которые можно совершить на сайте
			case 'glob_action':
			    $html_content = $this->global_actio_start();
			break;


			//Вызвано действие с переинсталяицей языковых переменных
			case 'lang_reinstal':
			    $this->lang_reinstall();
			    $kernel->pub_redirect_refresh('glob_action');
			    break;


			//Вызвано действие по удалению кеша сайта из папки "/cache"
			case 'flush_cache':
			    $this->flush_cache();
			    $kernel->pub_redirect_refresh('glob_action');
			    break;

            //Вызывает действие по простановке полных прав
			case 'set_full_cmod':
			    $mychmod = new manager_chmod();
			    $mychmod->files_set_acces(true);
			    $kernel->pub_redirect_refresh('glob_action');
			    break;

			//Вызывает действие по простановке ограниченных прав
			case 'set_lim_cmod':
			    $mychmod = new manager_chmod();
			    $mychmod->files_set_acces();
			    $kernel->pub_redirect_refresh('glob_action');
			    break;

			//Теперь обновление тоже без полных
			//прав, и потому мы сразу приступаем к обновлению
			case 'update_step_1':

			    $this->manager_update(SANTAFOX_VERSION);
			    $kernel->pub_redirect_refresh('update_step_4');
                break;
           case 'update_step_4':
                $this->lang_reinstall();
                $kernel->pub_redirect_refresh("info_site");
                break;
			case 'add_ip_ban':
				return $this->add_ip_ban();
			break;

			case 'ip_ban_save':
				return $this->ip_ban_save();
			break;

			case 'edit_ip_ban':
				return $this->edit_ip_ban();
			break;

			case 'update_ip_ban':
				return $this->update_ip_ban();
			break;


			case 'add_ip_ban':
				return $this->add_ip_ban();
			break;

			case 'save_ip_ban':
				return $this->save_ip_ban();
			break;

			case 'edit_ip_ban':
				return $this->edit_ip_ban();
			break;

			case 'update_ip_ban':
				return $this->update_ip_ban();
			break;

			case 'add_allow':
				return $this->add_allow();
			break;

			case 'edit_allow':
				return $this->edit_allow();
			break;

			case 'update_allow':
				return $this->update_allow();
			break;

			case 'save_allow':
				return $this->save_allow();
			break;

			case 'allow_delete':
				return $this->allow_delete();
			break;

			case 'page_fields':
				return $this->page_fields();
			break;

			case 'page_field_add':
				return $this->page_field_add();
			break;
			case 'page_field_save':
				return  $this->page_field_save();
			break;

			case 'page_field_del':
				$field = $kernel->pub_httpget_get('field');
				return $this->page_field_del($field);
			break;
			case 'save_fields_visible':

				return $this->save_fields_visible();
			break;

		}



	    return $html_content;
    }

    /**
     * Формирует форму, для редактирования параметров INI файла.
     *
     * @return string
     */
    function show_form_global_param()
    {
    	global $kernel;
    	$html = file_get_contents("admin/templates/default/edit_ini_file.html");
    	//Сразу пропишем текущее дествие для формы
    	$html = str_replace("[#form_aсtion#]", $kernel->pub_redirect_for_form("save_sys_prop"), $html);
		foreach ($this->parse_global_ini_file() as $key => $val)
		{
            $val = trim($val);
            if ($val=="true")
                $val="checked";
            elseif ($val=="false")
                $val="";
			$html = str_replace("[#".trim(strtolower($key))."_value#]", $val, $html);
		}
    	return $html;
    }


    /**
     * @return mixed
     */
    function show_form_sec_param()
    {
           	global $kernel, $sec_config;

        $root = $kernel->pub_site_root_get();



          $this->template = $kernel->pub_template_parse('admin/templates/default/edit_security_file.html');


            $source[]   = '%action%';
            $replace[]  = '/admin/index.php?action=set_left_menu&leftmenu=save_sec_prop';

            $html = $this->template['main'];


              $methods = array('GET', 'POST', 'COOKIE', 'SERVER');
              $attacks = array('xss', 'lfi', 'sql', 'php');





           //настройки блокировки
            $source[] = '%checked_block_tor%';
            $replace[] = ($sec_config['block_options']['block_tor'] == 1) ? 'checked = checked' : '';





          $block_matrix = $sec_config['block_options']['block_matrix'];



          foreach($block_matrix as $method_key => $method)
          {
              $method = str_split($method);

              foreach($method as $key => $check)
              {
                  $source[] = '%checked_block_'.strtolower($method_key) .'_'.$attacks[$key].'%';
                  $replace[] = ($check === '1') ?  'checked = checked' : '';
              }
          }






          if ($sec_config['rules_in_admin'] == 1)
          {


               $rules_tpl = $this->template['rules_in_admin'];


                $source_r[] = '%xss_rules%';      $replace_r[] = $sec_config['xss_rules'];
                $source_r[] = '%lfi_rules%';      $replace_r[] = $sec_config['lfi_rules'];
                $source_r[] = '%php_rules%';      $replace_r[] = $sec_config['php_rules'];
                $source_r[] = '%sql_rules%';      $replace_r[] = $sec_config['sql_rules'];


                $rules_tpl = str_replace($source_r, $replace_r, $rules_tpl);

                $source_r   = NULL;
                $replace_r  = NULL;

                $source[] = '%rules_in_admin%'; $replace[] = $rules_tpl;
          }
          else
          {
                       $source[] = '%rules_in_admin%'; $replace[] = '';
          }


          if ($sec_config['ignore_in_admin'] == 1)
          {


               $ignore_tpl = $this->template['ignore_in_admin'];


                $source_r[] = '%ignore_get%';           $replace_r[] = $sec_config['ignore_get'];
                $source_r[] = '%ignore_post%';          $replace_r[] = $sec_config['ignore_post'];
                $source_r[] = '%ignore_cookie%';        $replace_r[] = $sec_config['ignore_cookie'];
                $source_r[] = '%ignore_server%';        $replace_r[] = $sec_config['ignore_server'];

                $ignore_tpl = str_replace($source_r, $replace_r, $ignore_tpl);

                $source_r   = NULL;
                $replace_r  = NULL;


                $source[] = '%ignore_in_admin%'; $replace[] = $ignore_tpl;
          }
          else
          {
                       $source[] = '%ignore_in_admin%'; $replace[] = '';
          }


        $source[] = '%tags_filter%';    $replace[] = $sec_config['tags_filter'];


        //настройки бана

        $source[]   = '%checked_ban_tor%';
        $replace[]  = ($sec_config['ban_options']['ban_tor'] == 1) ? 'checked = checked' : '';

        $source[]   = '%checked_ban_subnet%';
        $replace[]  = ($sec_config['ban_options']['ban_subnet'] == 1) ? 'checked = checked' : '';

        $source[]   = '%checked_ban_forwarded_for%';
        $replace[]  = ($sec_config['ban_options']['ban_forwarded_for'] == 1) ? 'checked = checked' : '';



        $source[]   = '%checked_ban_admin_enter%';
        $replace[]  = ($sec_config['ban_options']['ban_admin_enter'] == 1) ? 'checked = checked' : '';

        $ban_matrix = $sec_config['ban_options']['ban_matrix'];

        foreach($ban_matrix as $method_key => $method)
        {
            $method = str_split($method);

            foreach($method as $key => $check)
            {
                $source[] = '%checked_ban_'.strtolower($method_key) .'_'.$attacks[$key].'%';
                $replace[] = ($check === '1') ?  'checked = checked' : '';
            }
        }


        //настройки извещаний


        $source[]   = '%checked_notice_mail%';
        $replace[]  = ($sec_config['notice_options']['notice_mail'] == 1) ? 'checked = checked' : '';


        $source[]   = '%checked_notice_sms%';
        $replace[]  = ($sec_config['notice_options']['notice_sms'] == 1) ? 'checked = checked' : '';

        $source[]   = '%checked_notice_push%';
        $replace[]  = ($sec_config['notice_options']['notice_push'] == 1) ? 'checked = checked' : '';

        $source[]   = '%checked_notice_admin_enter%';
        $replace[]  = ($sec_config['notice_options']['notice_admin_enter'] == 1) ? 'checked = checked' : '';

        $notice_matrix = $sec_config['notice_options']['notice_matrix'];

        foreach($notice_matrix as $method_key => $method)
        {
            $method = str_split($method);

            foreach($method as $key => $check)
            {
                $source[] = '%checked_notice_'.strtolower($method_key) .'_'.$attacks[$key].'%';
                $replace[] = ($check === '1') ?  'checked = checked' : '';
            }
        }


        //настройки входа в админку



        $crypt_methos = Array(
                        0=>'Без шифрования пароля',
                        6   =>  'Средний',
                        );

            $algos = hash_algos();

            if (in_array('sha256', $algos))
            {
                $crypt_methos[7] = 'Сложный';
            }

            if (in_array('sha512', $algos))
            {
                $crypt_methos[8] = 'Очень сложный';
            }

            if ($sec_config['login_options']['login_pass_crypt'] != '0')
            {
                $source[]   =  '%pass_disabled_value%';
                $replace[]  = 'disabled = "disabled" ';
            }
            else
            {
                $source[]   =  '%pass_disabled_value%';
                $replace[]  = '';
            }


            $option = '';
            foreach($crypt_methos as $key=> $method)
            {
                if ($key == $sec_config['login_options']['login_pass_crypt'])
                {
                    $option .= '<option value = '.$key.' selected = "selected" value = "'.$key.'">'.$method.'</option>';
                }
                else
                {
                    $option  .= '<option value = '.$key.' value = "'.$key.'">'.$method.'</option>';
                }
            }

        $source[] = '%options_crypt%';
        $replace[]  = $option;



        $source[]   = '%sectet_key_value%';
        $replace[]  =  $sec_config['login_options']['login_secret_key'];

        $source[]   = '%mail_value%';
        $replace[]  =  $sec_config['other_options']['other_mail'];

        $source[]   = '%push_login_value%';
        $replace[]  =  $sec_config['other_options']['other_push_login'];


        $source[]   = '%sms_login_value%';
        $replace[]  =  $sec_config['other_options']['other_sms_login'];

        $source[]   = '%other_phone%';
        $replace[]  =  $sec_config['other_options']['other_phone'];


        $source[]   = '%login_ip_value%';
        $replace[]  =  $sec_config['login_options']['login_ip'];




        $source[]   = '%checked_sms_auth%';
        $replace[]  = ($sec_config['login_options']['login_sms'] == 1) ? 'checked = checked' : '';


        $source[]   = '%checked_captcha_auth%';
        $replace[]  = ($sec_config['login_options']['login_captcha'] == 1) ? 'checked = checked' : '';


        //бан
        $source[]   = '%banned%';
        $replace[]  = $this->template['banned'];



		$banned_rows = $this->view_banned_table();


        $source[]   = '%banned_rows%';
        $replace[]  = $banned_rows;



		//антибан
		$source[]   = '%view_allow%';
		$replace[]  = $this->template['view_allow'];


		$allow_rows = $this->view_allow_table();

		$source[]   = '%allow_rows%';
		$replace[]  = $allow_rows;

        $dir = $kernel->pub_site_root_get().'/logs';


        $logs = Array('xss', 'lfi', 'sql', 'php', 'tor', 'ddos');

        foreach (glob($dir . "/*.txt") as $file)
        {
            $name = basename($file, '_log.txt');
            $size = filesize($file);

            $logs_array[$name] = $size;

        }


        $log_tpl = $this->template['ignore_in_admin'];

        foreach($logs as $log)
        {
            $source[]   = '%download_'.$log.'%';

            //если файл существует
            if (isset($logs_array[$log]))
            {

                $source2[] = '%get_url%';
                $replace2[] = 'index.php?action=set_left_menu&leftmenu=get_sec_log&log='.$log;

                $source2[] = '%del_url%';
                $replace2[] = 'index.php?action=set_left_menu&leftmenu=del_sec_log&log='.$log;

                $source2[] = '%filesize%';
                $replace2[] = $kernel->str_fsize($logs_array[$log]);

                //подсветка логов, чтобы автор понял, что пора бы почистить
                $source2[] = '%color%';

                //лог больше 10 мегов, следует чистить
                $replace2[]  =  ($logs_array[$log] > 102400) ? 'style = \'background-color:pink; background-image:none\'' : '';

                $replace[]  = str_replace($source2, $replace2, $this->template['log_button']);

                $source2 = NULL;
                $replace2 = NULL;
            }
            else
            {
                $replace[] = 'Пусто';
            }

        }
        $source[]   = '%login_logs%';


        $admin_login_file = 'logs/admin_login.txt';
        if (file_exists($admin_login_file))
        {
            $replace[]  = file_get_contents($admin_login_file);
        }
        else
        {
            $f = fopen($admin_login_file, 'w');
            fclose($f);
            $replace[] = '';
        }


        $html = str_replace($source, $replace, $html);

        return $html;
    }

     /**
     * Возвращает в виде массива, все настройки прописаные через define в ini файле.
     * @return array
     */
    function parse_global_ini_file()
    {
    	$content = file_get_contents('ini.php');
   		$str_preg  = "/define(?:\\s*)\\((.*)\\)(?:\\s*);/iU";

   		$array_define = array();
    	preg_match_all($str_preg, $content, $array_define);
    	$array_define = $array_define[1];

    	$ret = array();
    	foreach ($array_define as $val)
    	{
    		$tmp = explode(",",$val);
    		$new_key = $tmp[0];
    		$new_val = $tmp[1];
    		$new_key = str_replace("'", " ", $new_key);
    		$new_val = str_replace("'", " ", $new_val);
    		$new_key = str_replace('"', " ", $new_key);
    		$new_val = str_replace('"', " ", $new_val);

    		$ret[trim($new_key)] = trim($new_val);
    	}

    	//Добавим заплатку для старых версий, в которых может не быть как-их
    	//то парметров
    	$ret['ssl_connection'] = 'false';
    	$ret['webform_coding'] = 'false';
        if (!isset($ret['PRINT_MYSQL_ERRORS']))
            $ret['PRINT_MYSQL_ERRORS']='false';
        if (!isset($ret['IS_1251_TEMPLATES']))
            $ret['IS_1251_TEMPLATES']='false';
        if (!isset($ret['PAGE_FOR_404']))
            $ret['PAGE_FOR_404']='index';

        if (!isset($ret['HTTP_HOST']))
            $ret['HTTP_HOST']= $_SERVER['HTTP_HOST'];

    	return $ret;
    }

      /**
     * Сохраняет данные формы в INI файл.
     *
     */
    function save_sys_properties()
    {
    	global $kernel;
    	$array_data = array();
    	$post       = $kernel->pub_httppost_get();
        foreach ($post as $key => $val)
            $array_data[$key] = '"'.$val.'"';
        if (isset($array_data['time_creat']))
            $array_data['time_creat'] = "true";
        else
            $array_data['time_creat'] = "false";

        if (isset($array_data['generate_statistic']))
            $array_data['generate_statistic'] = "true";
        else
            $array_data['generate_statistic'] = "false";

    	if (isset($array_data['close_windows_on_save']))
    	    $array_data['close_windows_on_save'] = "true";
    	else
    	    $array_data['close_windows_on_save'] = "false";

    	if (isset($array_data['show_int_errore_message']))
    	    $array_data['show_int_errore_message'] = "true";
    	else
    	    $array_data['show_int_errore_message'] = "false";

    	if (isset($array_data['cached_page']))
    	    $array_data['cached_page'] = "true";
    	else
    	    $array_data['cached_page'] = "false";

    	if (isset($array_data['redir_www']))
    	    $array_data['redir_www'] = "true";
    	else
    	    $array_data['redir_www'] = "false";
		
    	if (isset($array_data['use_pretty_url']))
    	    $array_data['use_pretty_url'] = "true";
    	else
    	    $array_data['use_pretty_url'] = "false";
		
    	if (isset($array_data['ssl_connection']))
    	    $array_data['ssl_connection'] = "true";
    	else
    	    $array_data['ssl_connection'] = "false";

    	if (isset($array_data['webform_coding']))
    	    $array_data['webform_coding'] = "true";
    	else
    	    $array_data['webform_coding'] = "false";

    	if (isset($array_data['print_mysql_errors']))
    	    $array_data['print_mysql_errors'] = "true";
    	else
    	    $array_data['print_mysql_errors'] = "false";

    	if (isset($array_data['is_1251_templates']))
    	    $array_data['is_1251_templates'] = "true";
    	else
    	    $array_data['is_1251_templates'] = "false";

        if (isset($array_data['ftp_host']) && !empty($array_data['ftp_host']) && substr($array_data['ftp_host'],0,6)=="ftp://")
    	    $array_data['ftp_host'] = substr($array_data['ftp_host'],6);


        $array_data['SANTAFOX_VERSION'] = '"'.SANTAFOX_VERSION.'"';

    	if (!empty($array_data))
    	{
    		$str_ini_php = "";
    		foreach ($array_data as $key => $val)
    			$str_ini_php .= '    define("'.strtoupper($key).'", '.trim($val).')'.";\n";
			if (!empty($str_ini_php))
			{
			    $str_ini_php = "<?php\n    mb_internal_encoding(\"UTF-8\");\n".$str_ini_php."\n?>";
			    $kernel->pub_file_save($kernel->pub_site_root_get()."/ini.php", $str_ini_php);
				$this->edit_htaccess($array_data); // сохраняем правила ЧПУ в .htaccess
			}
    	}
    }




	function view_banned_table()
	{

		global $kernel;

		$sql = "SELECT *, INET_NTOA(`ip`) AS ip_adress, INET_NTOA(`forward_ip`) AS forward_ip_adress FROM ".$kernel->pub_prefix_get()."_banned";

		$rows = $kernel->db_get_list($sql);


		$banned_rows = '';
		foreach($rows as $row)
		{
			$source2 = NULL;
			$replace2 = NULL;

			$source2[]   = '%date_value%';
			$replace2[]  = $row['date_banned'];

			$source2[]   = '%date_unbanned_value%';
			$replace2[]  = $row['date_unbanned'];

			$source2[] = '%ip_value%';
			$replace2[] = $row['ip_adress'];

			$source2[] = '%forvarded_for_value%';
			$replace2[] = $row['forward_ip_adress'];

			$source2[] = '%user_agent_value%';
			$replace2[] = $kernel->pub_str_prepare_set(htmlentities($row['user_agent']));

			$source2[] = '%note_value%';
			$replace2[] = $row['note'];


			$source2[] = '%actions%';
			$replace2[] = '<a onclick="jspub_confirm(\'delete_ip&ip='.$row['ip_adress'].'\', \'Вы действительно хотите удалить этот ip адрес из бан листа?\')" href = "#"><img src = "/admin/templates/default/images/icon_delet.gif" alt ="удалить"></a>
				<a onclick="jspub_click(\'edit_ip_ban&ip='.$row['ip_adress'].'\');" href = "#"><img src="/admin/templates/default/images/icon_edit.gif" alt="Редактировать"></a>';


			$banned_rows .= str_replace($source2, $replace2, $this->template['banned_rows']);


		}
		return $banned_rows;
	}



	function view_allow_table()
	{

		global $kernel;

	//	$sql = "SELECT *, INET_NTOA(`ip`) AS ip_adress, INET_NTOA(`forward_ip`) AS forward_ip_adress FROM ".$kernel->pub_prefix_get()."_nobanned";

		$sql = "SELECT id, IFNULL(INET_NTOA(ip), host) as host, block, log, comment FROM ".$kernel->pub_prefix_get()."_nobanned";
		$rows = $kernel->db_get_list($sql);



		$allow_rows = '';
		foreach($rows as $row)
		{
			$source2 = NULL;
			$replace2 = NULL;


			$source2[] = '%host%';
			$replace2[] = $row['host'];

			$source2[] = '%block%';
			$replace2[] = ($row['block'] == '1') ? 'да' : 'нет';

			$source2[] = '%log%';
			$replace2[] = ($row['log'] == '1') ? 'да' : 'нет';

			$source2[] = '%comment%';
			$replace2[] = $row['comment'];


			$source2[] = '%actions%';
			$replace2[] = '<a onclick="jspub_confirm(\'delete_allow&id='.$row['id'].'\', \'Вы действительно хотите удалить этот ip адрес из бан листа?\')" href = "#"><img src = "/admin/templates/default/images/icon_delet.gif" alt ="удалить"></a>
				<a onclick="jspub_click(\'edit_allow&id='.$row['id'].'\');" href = "#"><img src="/admin/templates/default/images/icon_edit.gif" alt="Редактировать"></a>';


			$allow_rows .= str_replace($source2, $replace2, $this->template['allow_rows']);


		}
		return $allow_rows;
	}

    function get_arr_string_len($row)
    {
        static $length;

        if (empty($length))
        {
            $length = 0;
        }



        foreach ($row as $key => $value)
        {
            if (is_array($value))
            {
                $key_length = $this->get_arr_string_len($value);
            }
            else
            {
                $key_length =  strlen($key);
            }

            if ($key_length > $length)
            {
                $length = $key_length;
            }
        }
        return $length;
    }


    function gen_array_matrix($option)
    {
        global $kernel;

        $post       = $kernel->pub_httppost_get();

        $attacks = Array('xss', 'lfi', 'sql', 'php');
        $methods = Array('get', 'post', 'cookie', 'server');

        if (!empty($post[$option]))
        {
            foreach ($post[$option] as $method => $attack)
            {
                $nm = NULL;

                foreach ($attacks as $a)
                {
                    $nm[] = (isset($attack[$a])) ? '1' : '0';
                }
                $matrix[$method] = join('', $nm);

            }
        }
        else
        {
            foreach ($methods as $method)
            {
                $matrix[$method] = '0000';
            }
        }


        return $matrix;
    }


	/**
	 * Функция добавления бана
	 * @return mixed
	 */
	function add_ip_ban()
	{
		global $kernel;

		$this->template = $kernel->pub_template_parse('admin/templates/default/edit_security_file.html');

		$source[] 	= '%action%';
		$replace[]	= '/admin/index.php?action=set_left_menu&leftmenu=save_ip_ban';
		return str_replace($source, $replace, $this->template['add_ip_ban']);

	}


	/**
	 * Функция редактирования бана
	 * @return mixed
	 */
	function edit_ip_ban()
	{
		global $kernel;

		$ip = str_replace('*', '0', $_GET['ip']);


		$row = $kernel->db_get_record_simple('_banned', 'ip = '. sprintf("%u", ip2long($ip)));


		foreach($row as $key => $item)
		{
			$source[] 	= '%'.$key.'%';
			$replace[]	= $item;
		}

		$datetime = explode(' ', $row['date_banned']);

		$source[] = '%date%';
		$replace[] = $datetime[0];

		$source[] = '%time%';
		$replace[] = $datetime[1];

		$source[] = '%ip_adress%';
		$replace[] = $ip;

		$source[] 	= '%action%';
		$replace[]	= '/admin/index.php?action=set_left_menu&leftmenu=update_ip_ban';

		$this->template = $kernel->pub_template_parse('admin/templates/default/edit_security_file.html');


		return str_replace($source, $replace, $this->template['edit_ip_ban']);

	}


	/**
	 * Функция сохранения бана
	 * @return string
	 */
	function save_ip_ban()
	{

		global $kernel;

		$ip = str_replace('*', '0', $_POST['ip']);


		$ban = $kernel->db_get_list_simple('_banned', 'ip = '. sprintf("%u", ip2long($ip)));

		if (!empty($ban))
		{
			return $kernel->pub_httppost_response('такой ip адрес уже есть в базе!');
		}
		else
		{


			$rec['date_banned']		= date('Y-m-d h:i:s');
			$rec['ip'] 				= sprintf("%u", ip2long($ip));
			$rec['date_unbanned']	= $_POST['date_unblock']. ' '. $_POST['time_ublock'];
			$rec['note']			= $_POST['comment'];

			$insert = $kernel->db_add_record('_banned', $rec);

			return $kernel->pub_httppost_response('данные успешно сохранены', 'sec_prop');
		}
	}


	/**
	 * Обновление бана
	 * @return string
	 */
	function update_ip_ban()
	{

		global $kernel;

		$ip = str_replace('*', '0', $_POST['ip']);
		$ip = sprintf("%u", ip2long($ip));





			$rec['date_unbanned']	= $_POST['date_unblock']. ' '. $_POST['time_ublock'];
			$rec['note']			= $_POST['comment'];

			$insert = $kernel->db_update_record('_banned', $rec, "ip='".$ip."'");

			return $kernel->pub_httppost_response('данные успешно обновлены', 'sec_prop');

	}


	/**
	 * Добавление исключения
	 * @return mixed
	 */
	function add_allow()
	{
		global $kernel;

		$this->template = $kernel->pub_template_parse('admin/templates/default/edit_security_file.html');

		$source[] 	= '%action%';
		$replace[]	= '/admin/index.php?action=set_left_menu&leftmenu=save_allow';
		return str_replace($source, $replace, $this->template['add_allow']);
	}


	function save_allow()
	{
		global $kernel;

		//проверяем, это ip или хост
		$host = $kernel->pub_httppost_get('host');

		$sub_host = str_replace('*', '0', $host);
		if (filter_var($sub_host, FILTER_VALIDATE_IP))
		{

			$rec['ip'] = sprintf("%u", ip2long($sub_host));
		}
		else
		{
			$rec['host'] = $host;
		}

		if (!empty($_POST['block']))
		{
			$rec['block'] = $kernel->pub_httppost_get('block');
		}
		else
		{
			$rec['block'] = '0';
		}
		if (!empty($_POST['log']))
		{
			$rec['log'] = $kernel->pub_httppost_get('log');
		}
		else
		{
			$rec['log'] = '0';
		}


		$rec['comment']			= $kernel->pub_httppost_get('comment');

		$insert = $kernel->db_add_record('_nobanned', $rec);

		return $kernel->pub_httppost_response('данные успешно добавлены', 'sec_prop');
	}

	/**
	 * Редактирование исключения
	 * @return mixed
	 */
	function edit_allow()
	{
		global $kernel;


		$row = $kernel->db_get_record_simple('_nobanned',  'id = '.$kernel->pub_httpget_get('id'), 'id, IFNULL(INET_NTOA(ip), host) as host, block, log, comment');

		$row['checked_log'] = ($row['log'] != '0') ? 'checked = "checked"' : '';

		$row['checked_block'] = ($row['block'] != '0') ? 'checked = "checked"' : '';



		foreach($row as $key => $item)
		{
			$source[] 	= '%'.$key.'%';
			$replace[]	= $item;
		}


		$source[] 	= '%action%';
		$replace[]	= '/admin/index.php?action=set_left_menu&leftmenu=update_allow&id='.$row['id'];

		$this->template = $kernel->pub_template_parse('admin/templates/default/edit_security_file.html');


		return str_replace($source, $replace, $this->template['edit_allow']);

	}


	/**
	 * Обновление исключения
	 * @return string
	 */
	function update_allow()
	{
		global $kernel;

		//проверяем, это ip или хост
		$host = $kernel->pub_httppost_get('host');

		$sub_host = str_replace('*', '0', $host);
		if (filter_var($sub_host, FILTER_VALIDATE_IP))
		{
			$rec['ip'] = sprintf("%u", ip2long($sub_host));
		}
		else
		{
			$rec['host'] = $host;
		}

		if (!empty($_POST['block']))
		{
			$rec['block'] = $kernel->pub_httppost_get('block');
		}
		else
		{
			$rec['block'] = '0';
		}
		if (!empty($_POST['log']))
		{
			$rec['log'] = $kernel->pub_httppost_get('log');
		}
		else
		{
			$rec['log'] = '0';
		}


		$rec['comment']			= $kernel->pub_httppost_get('comment');

		$insert = $kernel->db_update_record('_nobanned', $rec, "id=".$kernel->pub_httpget_get('id'));

		return $kernel->pub_httppost_response('данные успешно обновлены', 'sec_prop');
	}

	function save_sec_prop()
    {
        global $kernel, $sec_config;

        $root = $kernel->pub_site_root_get();

       // include_once($root."/security.php");



        //сначала заберем стаые настройки
        $new_config = $sec_config;

        $post       = $kernel->pub_httppost_get();


        //блокировка
        $new_config['block_options']['block_tor']       = (!empty($post['block_tor'])) ? '1' : '0';
        $new_config['block_options']['block_matrix']    =  $this->gen_array_matrix('block');

        //бан
        $new_config['ban_options']['ban_tor']           = (!empty($post['ban_tor']))            ? '1' : '0';
        $new_config['ban_options']['ban_subnet']        = (!empty($post['ban_subnet']))         ? '1' : '0';
        $new_config['ban_options']['ban_forwarded_for'] = (!empty($post['ban_forwarded_for']))  ? '1' : '0';
        $new_config['ban_options']['ban_admin_enter']   = (!empty($post['ban_admin_enter']))    ? '1' : '0';
        $new_config['ban_options']['ban_matrix']        =  $this->gen_array_matrix('ban');


        //информирование
        $new_config['notice_options']['notice_mail']        = (!empty($post['notice_mail']))         ? '1' : '0';
        $new_config['notice_options']['notice_sms']         = (!empty($post['notice_sms']))          ? '1' : '0';
        $new_config['notice_options']['notice_push']        = (!empty($post['notice_push']))         ? '1' : '0';
        $new_config['notice_options']['notice_admin_enter'] = (!empty($post['notice_mail']))         ? '1' : '0';
        $new_config['notice_options']['notice_matrix']      = $this->gen_array_matrix('notice');


        $new_config['login_options']['login_pass_crypt'] = (!empty($post['login_pass_crypt'])) ? $post['login_pass_crypt']  : $sec_config['login_options']['login_pass_crypt'];
        $new_config['login_options']['login_secret_key'] = (!empty($post['login_secret_key'])) ? $post['login_secret_key']  : $sec_config['login_options']['login_secret_key'];


        if (!empty($post['login_pass_crypt']))
        {

            $new_config['login_options']['login_pass_crypt'] = $post['login_pass_crypt'];
        }


        $new_config['login_options']['login_sms']        =  (!empty($post['login_sms']))         ? '1' : '0';
        $new_config['login_options']['login_captcha']    =  (!empty($post['login_captcha']))         ? '1' : '0';
        $new_config['login_options']['login_ip']         =  $post['login_ip'];


        //прочие настройки
        $new_config['other_options']['other_mail']          = $post['other_mail'];
        $new_config['other_options']['other_sms_login']     = $post['other_sms_login'];
        $new_config['other_options']['other_push_login']    = $post['other_push_login'];

        $new_config['other_options']['other_phone']         = $post['other_phone'];



ob_start();
var_export($new_config);
$content = ob_get_contents();
ob_end_clean();
$content =
'<?php
/*
* Файл настроек безопасности.
* Внимание, изменение через конфигурационный файл нежелательно, рекомендуем вопсользоваться админкой
* В частности данные по изменению пароля и телефона - сделует делать только в админке
*/
$sec_config = '.$content.';
?>';
       if (!$kernel->pub_file_save($kernel->pub_site_root_get()."/security.php", $content))
       {
           die ('Проверьте права на файл security.php!');
       }
       //записываем все это дело только тогда, когда есть права
        else
        {

            if (!empty($post['login_pass_crypt']))
            {

                if($sec_config['login_options']['login_pass_crypt'] != $post['login_pass_crypt'])
                {
                    //если пароли хранятся в открытом виде, забираем и шифруем
                    if($sec_config['login_options']['login_pass_crypt'] == '0')
                    {
                        $sql = "SELECT * FROM " . $kernel->pub_prefix_get() . "_admin";

                        $admins = $kernel->db_get_list($sql);

                        foreach ($admins as $item)
                        {
                            $pass = $item['pass'];

                            $rec['pass'] = $kernel->pass_hash($pass, $new_config['login_options']['login_pass_crypt'], $new_config['login_options']['login_secret_key']);


                            if($rec['pass'])
                            {
                                $condition = " login = '" . $item['login'] . "'";
                                $kernel->db_update_record('_admin', $rec, $condition);
                            }
                            else
                            {
                                $error = 'Не могу изменить пароль!';
                            }

                        }


                    }

                }
            }

            if (!empty($post['htpasswd_pass']))
            {

                //генерируем htpasswd пароль
                $htpasswd = 'admin:' . $kernel->md5_apr($post['htpasswd_pass']);
                $kernel->pub_file_save($kernel->pub_site_root_get() . "/admin/.htpasswd", $htpasswd);


                $htaccess = file_get_contents($kernel->pub_site_root_get() . "/admin/.htaccess");

                //ищем вхождение c информацией о htpasswd файле
                if (preg_match('#^AuthUserFile(.*?)$#u', $htaccess))
                {
                    $htaccess = preg_replace('#^AuthUserFile(.*?)$#u', $kernel->pub_site_root_get() . '/admin/.htpasswd', $htaccess);
                }
                //не найдено? значит создадим!
                else
                {
                    $htaccess .=
                        '
AuthType Basic
AuthName "Admin protected Area!"
AuthUserFile ' . $kernel->pub_site_root_get() . '/admin/.htpasswd
Require valid-user
<Files .htpasswd>
   deny from all
</Files>';
                }

                $kernel->pub_file_save($kernel->pub_site_root_get() . "/admin/.htaccess", $htaccess);
                $kernel->pub_file_save($kernel->pub_site_root_get() . "/admin/.htpasswd", $htpasswd);
            }
        }



        $kernel->pub_redirect_refresh_reload("sec_prop");
    }


    function delete_ip()
    {
        global $kernel;

        $ip = $_GET['ip'];

        $sql = "DELETE FROM ".$kernel->pub_prefix_get()."_banned WHERE ip = ". sprintf("%u", ip2long($ip));

        $kernel->runSQL($sql);

        $kernel->pub_redirect_for_form("sec_prop");

    }


	function delete_allow()
	{
		global $kernel;



		$sql = "DELETE FROM ".$kernel->pub_prefix_get()."_nobanned WHERE id = ". $kernel->pub_httpget_get('id');

		echo $sql;


		$kernel->runSQL($sql);

		$kernel->pub_redirect_refresh_reload("sec_prop");



	}

    function get_sec_log()
    {

        global $kernel;
        $logs =  Array('xss', 'lfi', 'sql', 'php', 'tor', 'ddos');
        $files =  Array('xss_log.txt', 'lfi_log.txt', 'sql_log.txt', 'php_log.txt', 'tor_log.txt', 'ddos_log.txt');
        $files =  array_combine($logs, $files);


        if (isset($_GET['log']))
        {
            $log_file = $files[$_GET['log']];
            $filename = $kernel->pub_site_root_get() . "/logs/" . $log_file;

            if(file_exists($filename))
            {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Accept-Ranges: bytes');
                header('Content-Disposition: attachment; filename=' . $log_file);
                header('Content-Length: ' . filesize($filename));

                echo  file_get_contents($filename);
                die();
            }
        }
    }

    function del_sec_log()
    {
        global $kernel;
        $logs =  Array('xss', 'lfi', 'sql', 'php', 'tor', 'ddos');
        $files =  Array('xss_log.txt', 'lfi_log.txt', 'sql_log.txt', 'php_log.txt', 'tor_log.txt', 'ddos_log.txt');
        $files =  array_combine($logs, $files);

        $log_file = $files[$_GET['log']];
        $filename = $kernel->pub_site_root_get() . "/logs/" . $log_file;

        if(file_exists($filename))
        {
            unlink($filename);
        }
        $kernel->pub_redirect_refresh_reload("sec_prop");

    }

    function show_form_info()
    {
    	global $kernel;

    	//Зачитаем структуру файлов и узнаем версии
    	$html = $kernel->pub_template_parse("admin/templates/default/info_sait.html");
    	$html = $html['body'];
    	$html = str_replace('[#SERVER_SOFTWARE#]', $_SERVER['SERVER_SOFTWARE'], $html);
    	$html = str_replace('[#SERVER_ADDR#]', $_SERVER['SERVER_ADDR'], $html);
    	$html = str_replace('[#REMOTE_ADDR#]', $_SERVER['REMOTE_ADDR'], $html);
    	$html = str_replace('[#version_kernel#]', SANTAFOX_VERSION, $html);


        //Проверим возможность скриптом менять права всех файлах сайта
        $result = '--';

        $html = str_replace('[#SERVER_CAN_CHMOD_SCRIPT#]', $result, $html);

        //Теперь проверим возможность подключения к FTP
        $html = str_replace('[#SERVER_CAN_FTP_CONNECT#]', $result, $html);

        $descript = $this->update_get(SANTAFOX_VERSION);
        switch($descript)
        {
            case -2:
                $descript = '[#admin_glob_prop_update_no_uplink#]';
                break;
            case -1;
                $descript = '[#admin_glob_prop_update_no_update#]';
                break;
            case -3;
                $descript = '[#admin_glob_prop_modified_version_no_update#]';
                break;
            default:
                $descript = '<p><button onclick="jspub_click(\'update_step_1\');">Обновить</button></p>'.$descript;//@todo move to template
                break;
        }
        $html = str_replace('[#new_version_info#]', $descript, $html);
    	return $html;
    }


    /**
     * Возвращает описание доступного обновления, если оно есть
     *
     * @param string $version Строка типа #.# ,где # - число
     * @param boolean $only_code
     * @return string
     */

    function update_get($version, $only_code = false)
    {
        if (preg_match('~m$~i',$version))
            return -3;

        if ($only_code && isset($_SESSION['vars_kernel']['need_update_santa']))
            return $_SESSION['vars_kernel']['need_update_santa'];


        $content = $this->file_get($version, "request");

        //Сообщение о том что сервер не доступен
        $result = -2;
        if ($content != false)
        {
            $temp_files = explode($this->manager_update_delimiter, $content);

            if ((isset($temp_files[0])) && (isset($temp_files[1])))
            {
                $new_version = $temp_files[0];
                //Проверка на совпадение версий
                if ($version == $new_version)
                    $result = -1;
                else
                {
                    $description = $temp_files[1];
                    if ($only_code)
                        $result = 1;
                    else
                        $result = $description;
                }
            }
        }
        $_SESSION['vars_kernel']['need_update_santa'] = $result;

        return $result;
    }


    /**
     * Производит процесс обновления файлов в системе
     *
     * @param string $version Текущая версия движка
     * @return string
     */
    function manager_update($version)
    {
        global $kernel;

        $files = array();
        $root = $kernel->pub_site_root_get();

        $content = $this->file_get($version);
        if ($content === false)
            return false;


        $temp_files = explode($this->manager_update_delimiter, $content);
        $new_version = $temp_files[0];

        //Получаем имена файлов, которые нужно переписать
        unset($temp_files[0], $content);

        foreach ($temp_files AS $file)
        {

            preg_match("/\\{\\@(.*?)\\@\\}(.*)/is", $file, $result);
            $filename = $result[1];
            $files[$filename] = $result[2];
        }

        //Определим модули, которые есть у клиента
        $modules_arr = array();
        $dir = opendir($root."/modules");
        while ($file = readdir($dir))
        {
            if (($file != ".") && ($file != ".."))
            {
                if (is_dir($root."/modules/".$file))
                    $modules_arr[] = "^/modules/".$file;
            }
        }

        //Уберем из апдейта те модули, которых нет у клиента
        $need_reinstal = array();
        $modules_str = implode("|", $modules_arr);
        foreach ($files AS $tmp_filename => $null)
        {
            if (preg_match("|^/modules/([a-z0-9_-]+)\\.*|i", $tmp_filename, $tmp_arr))
            {
                $need_reinstal[$tmp_arr[1]] = $tmp_arr[1];
                if (!preg_match("@".$modules_str."@i", $tmp_filename))
                    unset($files[$tmp_filename]);
            }
        }

        //Собственно начнем процесс апдейта
        $upd_temp_dir = "/upload/update".$new_version;
        $kernel->pub_file_dir_create($root.$upd_temp_dir);

        // Сохраняем во временную папку
        //прежде всего создадим в необходимые дирректории во врменной папке
        $this->update_dir_create($files, $upd_temp_dir);

        $html = '';
        foreach ($files AS $path => $content)
        {
            if (!$kernel->pub_file_save($upd_temp_dir.$path, $content))
                $html .= $this->error_message_get($upd_temp_dir.$path);
            else
                $html .= 'Upload file: '.$path.'<br>';
        }

        //Файлы получены и лежат в папке update
        //Запустим инструкции перед копированием
        error_reporting(E_ALL);
        include_once($root.$upd_temp_dir."/_update.php");

        $update = new site_update();
        $update->start();

        unset($files["/_update.php"], $files["/_description.html"]);
        reset($files);

        //Теперь, нужно пройтись и создать в случае необходимости директории уже в самом сайте
        $this->update_dir_create($files, "/");

        //Непосредственно копирование файлов
        $errore_copy = false;
        foreach ($files AS $path => $content)
        {
            if (!$kernel->pub_file_copy($upd_temp_dir.$path, $root.$path))
            {
                $errore_copy = true;
                $html .= "Ошибка копирования <i>".$upd_temp_dir.$path."</i> в <i>".$root.$path."</i><br>";
            }
            else
                $html .= 'Copy: '.$root.$path.'<br>';
        }
        //Инструкции пост копирования
        $update->end();

        $array_modules = $kernel->pub_modules_get();
        $manager_modules = new manager_modules();
        foreach ($array_modules AS $modul_id => $modul_val)
        {
            if ((empty($modul_val['id_parent'])) && (isset($need_reinstal[$modul_id])))
                $manager_modules->reinstall_module($modul_id);
        }

        //Укажем новую версию в ini файле если не было ошибок в обновлении
        if ($errore_copy)
        {
            $kernel->debug($html, true);
            die(0);
        }
        if ($ini_php = file_get_contents($root."/ini.php"))
        {
            $ini_php = str_replace('define("SANTAFOX_VERSION", "'.$version.'");', 'define("SANTAFOX_VERSION", "'.$new_version.'");', $ini_php);
            $kernel->pub_file_save("/ini.php", $ini_php);
            $kernel->pub_file_delete($root.$upd_temp_dir,true);
        }
        else
            $html .= $this->error_message_get('Отсутствует файл ini.php');
        return $html;
    }


    function file_get($version, $request="update")
    {
        $file_content = "";
        if (!($resource = @fopen("http://update.santafox.ru/?ver=".$version."&request=".$request, "r")))
            return false;
        while (!feof($resource))
        {
            $file_content .= fread($resource, 8192);
        }
        fclose($resource);
        if (!preg_match("|".$this->manager_update_delimiter."|", $file_content))
            return false;

        return $file_content;
    }


    function error_message_get($file)
    {
        return "ERROR! (".$file.")<br>";
    }


    /**
     * Сохдаёт папки, которые возможно появились в обновлении
     *
     * @param array $files
     * @param string $link
     * @return boolean
     */
    function update_dir_create($files, $link)
    {
    	global $kernel;

    	$arr = array();
    	//Сначала создадаим список уникальных директорий
    	foreach ($files as $key => $val)
    	{
    		$path = pathinfo($key);
	        $path = $path['dirname'];
	        $arr[$path] = $path;
    	}
    	//Теперь будем создавать директории
    	$result = true;
    	foreach ($arr as $val)
    	{
    	   $result = $kernel->pub_dir_create_in_files($link.$val, true);
    	}
    	return $result;
    }

    function global_actio_start()
    {
        $html = file_get_contents('admin/templates/default/global_actions.html');
        return $html;
    }

    /**
     * Функция проводит переинсталяцию существующих языковых переменных
     *
     * Обновляются как языковые переменные на уровне ядра иак и на уровне модулей
     */
    function lang_reinstall()
    {
        $m_table = new mysql_table();

        $modul = new manager_modules();
        $arr = $modul->return_modules();

        //Определим тем записи, которые нам нужно оставить
        //А оставить нам нужно названия модуля для разных языков
        $lang_save = array();
        foreach ($arr as $val)
        {
            $lkey = $val['caption'];
            $lkey = str_replace("[#", "", $lkey);
            $lkey = str_replace("#]", "", $lkey);
            $lang_save[] = $lkey;
        }

        $m_table->lang_all_clear($lang_save);
        //Основные языковые переменные
        $m_table->add_langauge('include/install/lang');

        foreach ($arr as $key => $val)
        {
            if (!empty($val['id_parent']))
                continue;

            //Это модуль родитель. Его языковые переменые и проинсталируем
            $m_table->add_langauge('modules/'.$key.'/lang');
        }
    }


    /**
     * Функция очистки кеша сайта
     *
     * Производит очистку папки "/cache" от файлов с кэшем сайта
     */
    function flush_cache() {
		$caches = glob('cache/*');
		if (count($caches) > 0) {
			foreach ($caches as $cache_file) {
				if (file_exists($cache_file)) {
					unlink($cache_file);
				}
			}
		}
    }
	
    /**
     * Функция редактирования правил .htaccess сайта
     *
     * Производит добавление/удаление правил в корневой .htaccess сайта (пока нужна только для работы ЧПУ)
	 * 
     */
	function edit_htaccess($options)
    {
		global $kernel;
		$htaccess = file_get_contents($kernel->pub_site_root_get() . "/.htaccess");
		
		if(empty($htaccess))
			return false;
		
		if ($options['use_pretty_url']=="true")
		{
			$pretty_urls_rules = 'RewriteEngine On

### Pretty URL ###
# 301-редирект с добавлением слеша в конец ЧПУ
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_URI} !-f
RewriteCond %{REQUEST_URI} !/$
RewriteCond %{REQUEST_URI} !.html$
RewriteRule (.+) $1/ [R=301,L]

# ЧПУ, исключения
RewriteRule ^components/captcha/captcha.php components/captcha/captcha.php [QSA,L]
RewriteCond %{REQUEST_URI} (components|.css|.js|.jpg|.jpeg|.png|.gif|.pdf|.ttf|.eot|.svg|.woff|.woff2)
RewriteRule ^(.+) $1 [QSA,L]

# ЧПУ, базовые правила
RewriteRule ^([a-z0-9_-]+)/page-(\d+)/ index.php?sitepage=$1&page=$2 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/$ index.php?sitepage=$1 [QSA,L]

# ЧПУ для модуля "Галерея"
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-gcat(\d+)/page-(\d+)/$ index.php?sitepage=$1&gcat=$3&page=$4 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-gcat(\d+)/$ index.php?sitepage=$1&gcat=$3 [QSA,L]

# ЧПУ для модуля "Новости"
RewriteRule ^([a-z0-9_-]+)/start-([0-9-]+)/stop-([0-9-]+)/$ index.php?sitepage=$1&start=$2&stop=$3 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/start-([0-9-]+)/stop-([0-9-]+)/page-(\d+)/$ index.php?sitepage=$1&start=$2&stop=$3&page=$4 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/date-([0-9-]+)/page-(\d+)/$ index.php?sitepage=$1&date=$2&page=$3 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/date-([0-9-]+)/$ index.php?sitepage=$1&date=$2 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-p(\d+)\.html$ index.php?sitepage=$1&id=$3 [QSA,L]

# ЧПУ для модуля "Каталог"
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-c(\d+)/([a-z0-9_-]+)-i(\d+)\.html$ index.php?sitepage=$1&cid=$3&itemid=$5 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-i(\d+)\.html$ index.php?sitepage=$1&itemid=$3 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-c(\d+)/page-(\d+)/$ index.php?sitepage=$1&cid=$3&page=$4 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-c(\d+)/ index.php?sitepage=$1&cid=$3 [QSA,L]

# ЧПУ для модуля "Вопросы и ответы"
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-a(\d+)/ index.php?sitepage=$1&a=2&b=$3 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-q(\d+)/ index.php?sitepage=$1&a=3&b=$3 [QSA,L]

# ЧПУ для модуля "Поиск по сайту"
RewriteRule ^([a-z0-9_-]+)/(.+)/page-(\d+)/ index.php?sitepage=$1&search=$2&page=$3 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/(.+)/ index.php?sitepage=$1&search=$2 [QSA,L]
### End of Pretty URL ###';

			// проверяем, нет ли уже таких правил ЧПУ в файле .htaccess
			if (!preg_match('~### Pretty URL ###~', $htaccess)) 
				if (preg_match('~RewriteEngine On~', $htaccess))
					$htaccess = str_replace('RewriteEngine On', $pretty_urls_rules, $htaccess);
		}
		elseif ($options['use_pretty_url']=="false")
		{
		// значит опция ЧПУ отключена и необходимо убрать соответсвующие правила из .htaccess
			// проверяем наличие правил ЧПУ в .htaccess
			if (preg_match('~### Pretty URL ###~', $htaccess)) 
			{
				$pattern = '~### Pretty URL ###([\s\S]*?)### End of Pretty URL ###~';
				$htaccess = preg_replace($pattern, '', $htaccess);
				$htaccess = str_replace("\r\n\r\n\r\n", "\r\n", $htaccess);
			}
		} // значит опция ЧПУ включена и необходимо добавить соответсвующие правила в .htaccess
		
		$kernel->pub_file_save($kernel->pub_site_root_get() . "/.htaccess", $htaccess);
		return true;
	}



	function page_fields()
	{
		global $kernel;


		if (isset($_GET['menu_update']))
		{
			echo "<script>update_left_menu('page_fields');</script>";
		}


		$template = $kernel->pub_template_parse('admin/templates/default/properties_fields.html');

		$fields =  $kernel->db_get_list_simple('_page_fields', true);


		$line = $template['field'];

		$content = '';
		foreach($fields as $item)
		{


			//неудаляемые поля
			if ($item['required'] == '1')
			{
				$action_del =  '';
				$action_edit = '';
			}
			else
			{

				$link  = 'page_field_del&field='.$item['id'];

				$action_del =  $template['action_field'];
				$action_del = str_replace("%action_del%", $link, $action_del);

				//	$action_edit =  str_replace("%action_edit%", $link, $action_edit);
			}

			$types = array(
				'string'	=> 'Строка',
				'file'		=> 'Файл',
				'select'	=> "Выбор значения",
				'checkbox'	=> 'Галочка',
				'pagesite'	=> 'Раздел на сайте',
				'date'		=> 'Время',
				'textarea'	=> 'Текст');

			//переменные из шаблона


			unset($row);


			$row['id'] = $item['id'];

			$row['is_checked'] = ($item['menu_show'] == '1') ? ' checked' : '';
			$row['name'] = 'menu_show';

			$check_menu = $kernel->tpl_replace($row, $template['check']);




			$row['is_checked'] = ($item['way_show'] == '1') ? ' checked' : '';
			$row['name'] = 'way_show';

			$check_way = $kernel->tpl_replace($row, $template['check']);

			$row['is_checked'] = ($item['admin_show'] == '1') ? ' checked' : '';
			$row['name'] = 'admin_show';


			$check_admin = $kernel->tpl_replace($row, $template['check']);


			 $row  = array(
				'title_value' 	=> $item['name'],
				'type_value'	=> $types[$item['type']],
				'action_del'	=> $action_del,
				'check_menu'	=> $check_menu,
				'check_way'		=> $check_way,
				'check_admin'	=> $check_admin
			);


			$content .= $kernel->tpl_replace($row, $line);


		}


		$html =  $template['main'];
		$html = str_replace('%fields%', $content, $html);
		$html = str_replace('%save_action%', $kernel->pub_redirect_for_form('save_fields_visible'), $html);

		return $html;
	}

	function page_field_add()
	{
		global $kernel;

		$template = $kernel->pub_template_parse('admin/templates/default/properties_fields.html');

		$html = $template['add_field'];

		$form_action =    $kernel->pub_redirect_for_form('page_field_save');

		$html = str_replace('%form_action%', $form_action, $html);

		return $html;
	}

	function page_field_save()
	{
		global $kernel;

		$rec = $_POST;

		if (empty($rec['name']) or empty($rec['type']) or empty($rec['id']))
		{
			return $kernel->pub_httppost_response('Заполните все поля!');
		}
		elseif(!preg_match("/[A-Za-z0-9](.*)$/i", $rec['id']))
		{
			return   $kernel->pub_httppost_response("id поля содержит недопустимые символы!");
		}
		else
		{
			$kernel->db_add_record('_page_fields', $rec);



			return $kernel->pub_httppost_response('Данные успешно сохранены', 'page_fields');
		}
	}


	function page_field_del($field)
	{
		global $kernel;

		$sql = "DELETE FROM `".$kernel->pub_prefix_get()."_page_fields` WHERE id = '".$field."'";

		$kernel->runSQL($sql);


		return $kernel->pub_http_alert('Данные успешно удалены', 'page_fields');


	}

	function save_fields_visible()
	{

		global $kernel;

		$fields = $kernel->db_get_list_simple('_page_fields', 'true');

		//обновляем видимость
		foreach($fields as $item)
		{
			$item['way_show']	= (!empty($_POST['way_show'][$item['id']]))		? $_POST['way_show'][$item['id']] 	: $item['way_show'];
			$item['menu_show']	= (!empty($_POST['menu_show'][$item['id']]))	? $_POST['menu_show'][$item['id']] 	: $item['menu_show'];
			$item['admin_show']	= (!empty($_POST['admin_show'][$item['id']]))	? $_POST['admin_show'][$item['id']] : $item['admin_show'];

			$kernel->db_update_record('_page_fields', $item, "id = '". $item['id']."'");

		}

		return $kernel->pub_httppost_response('Данные успешно сохранены', 'page_fields');
	}


}