<?php
/**
 * Веб-фаервол для Santafox CMS
 *
 * @author Rinat <hello@hellosite.org>
 * @link http://hellosite.org
 *
 */
class security
{
	public $template = array();

	function __construct()
	{

		global $kernel, $sec_config;

		include_once $kernel->pub_site_root_get().'/security.php';

		$this->config = $sec_config;

		//если это исключение из правил, типа поисковый бот, то в первую очередь смотрим и пропускаем если что

		//если пользователь заблокирован, не дать ему что-то сделать

		if (!$this->is_nobanned())
		{
			if ($this->is_banned())
			{
				header("HTTP/1.1 500 Internal Server Error");
				$this->set_block('Доступ к сайту заблокирован!');
			}
		}

		if (!$kernel->is_backend())
		{
			$this->input_guard();
			$this->input_filter();
		}
		else
		{
			//@todo сделать также фильтрацию для админки
			// $this->backend_guard();
		}

	}


	/**
	 *  Функция для защиты от дураков и детей
	 */
	public function input_guard()
	{
		global $kernel;

		$_SESSION['user_ips'][$_SERVER['REMOTE_ADDR']] = $_SERVER['REMOTE_ADDR'];

		//проверка от смены ip адреса в рамках одной сессии или использование сетей tor
		if (!empty($_SESSION['ip']) && ($_SESSION['ip'] !== $_SERVER['REMOTE_ADDR']))
		{

			$this->break_off('', 'ip', $_SERVER['REMOTE_ADDR'], 'tor');
		}

		$_SESSION['ip'] = $kernel->pub_user_ip_get();

		$methods = array('get', 'post', 'cookie', 'server');
		$types_attack = array('xss', 'lfi', 'sql', 'php');

		foreach($methods as $method_type)
		{
			switch($method_type)
			{
				case 'get'      : $rows = $_GET; break;
				case 'post'     : $rows = $_POST; break;
				case 'cookie'   : $rows = $_COOKIE; break;
				case 'server'   : $rows = $_SERVER; break;
			}

			if (!empty($rows))
			{
				$this->input_guard_array($rows, $types_attack, $method_type);
			}
		}
	}


	/**
	 * Рекурсия для функции защиты от дураков и детей
	 * @param $rows - массив переменных, необходимых для проверки
	 * @param $types_attack - типы атак (xss, php injection и т.п.)
	 * @param string $method_type - суперглобальные переменные (get, post и т.п.)
	 */
	function input_guard_array($rows, $types_attack,$method_type='')
	{

		global $kernel;
		static $match, $recurs, $ignore, $callback;

		if (empty($recurs))
		{
			$recurs = 1;
		}
		else
		{
			$recurs++;
		}

		//если уровень вложенности слишком большой, то что-то не так. Блокируем!
		if ($recurs > 15)
		{
			$this->break_off($method_type, '', '', 'ddos');
		}


		//чтобы таюблицы по много раз не читались
		if (empty($match))
		{
			//подозрения на XSS, CSRF  и подозрительные названия
			$match['xss'] = $this->config['xss_rules'];

			//подозрения на LFI, traversal, PHP jnjection и подозрительные названия
			$match['lfi'] = $this->config['lfi_rules'];

			//подозрения на PHP jnjection и подозрительные названия
			$match['php'] = $this->config['php_rules'];

			if (in_array('sql', $types_attack))
			{
				if (empty($tables))
				{
					//сначала забирем все таблицы
					$tables = $kernel->db_get_tables();
					$tables2 = str_replace($kernel->pub_prefix_get(), 'sf', $tables);

					$tables = array_merge($tables, $tables2);

					$tables     = join('|', $tables);
				}
				$match['sql'] = $this->config['sql_rules'];
				$match['sql'] = str_replace('%tables%', $tables, $match['sql']);
			}


			$ignore_arrays = array('post', 'get', 'cookie', 'server');

			foreach($ignore_arrays as $item)
			{
				$name = 'ignore_'.$item;
				// для удобства и производительности - поиск по ключу
				if (!empty($this->config[$name]))
				{
					$conf_array = explode(',',  str_replace(' ', '', $this->config[$name]));
					$ignore[$item] = array_combine($conf_array, $conf_array);
					$conf_array =NULL;
				}
			}

			$callback = array(
				'server' =>  array('HTTP_REFERER'=>array('php' =>array('urldecode' => array('full'=>1))))
			);
		}

		$j = 0;
		foreach($rows as $key=> $value)
		{

			if (($value == null)  or  ($value == false) or (is_integer($value)) or  (!isset($ignore[$method_type][$key])))
			{
				//если передается очень много параметров
				if ($j > 200)
				{
					$this->break_off($method_type, $key, $value, 'ddos');
				}

				//если не массив, значит обрабатываем как строку
				if (!is_array($value))
				{

					//проверка на кодировку
					/*
						if ($method_type == 'GET')
						{
						   $charset =  mb_detect_encoding($value);

						   //зачем вы нам подсовываете неправильную кодировку!
						   if ($charset != DEFAULT_CHARSET)
						   {
										   $this->break_off($method_type, $key, $value, 'charset');
						   }

						}
					*/

					//проходим только по тем типам аттак, которые указаны
					foreach($types_attack as $attack)
					{

						//если указано - обрабатывать эту переменную определенной функцией
						if (isset($callback[$method_type][$key][$attack]))
						{
							if (is_array($callback[$method_type][$key][$attack]))
							{
								$user_func =  key($callback[$method_type][$key][$attack]);
							}
							else
							{
								$user_func = $callback[$method_type][$key][$attack];
							}

							$value = call_user_func(array('security', $user_func), $method_type, $key, $value, $attack);

						}

						if (preg_match($match[$attack], $value))
						{
							$this->break_off($method_type, $key, $value, $attack);
						}
					}
					$user_func = NULL;
				}
				else
				{
					//если данные представляют из себя массив, делаем рекурсию
					$this->input_guard_array($value, $types_attack, $method_type);
				}
			}
			$j++;

		}

	}


	/**
	 * Функция для произведения каких-либо действий в случае, если обнаружена попытка атаки (извещание, лог, блокировка, бан и пр)
	 * @param $type         -   метод (get, post и т.п.)
	 * @param $key          -  ключ (например HTTP_HOST - ключ из суперглобальной переменной $_SERVER
	 * @param $value        -  знаение (код самой атаки)
	 * @param $attack_type  -  тип атаки (xss, php injection и т.п.)
	 */
	function break_off($type, $key, $value, $attack_type)
	{



		global $kernel;

		static $nb_config;

		$this->break = new stdClass();
		$this->break->type          =   $type;
		$this->break->key           =   $key;
		$this->break->value         =   $value;
		$this->break->attack_type   =   $attack_type;

		$type = strtolower($type);

		//проверяем, что написано в конфиге
		$attacks =  Array('xss' => '0', 'lfi'=>'1', 'sql'=>'2', 'php' =>'3');


		if (empty($nb_config))
		{

			//проверяем хост
			$host = gethostbyaddr($_SERVER['REMOTE_ADDR']);

			// с введением многосимвольных доменов, может быть что-то не предвиденное, поэтому вырезаем все на всякий случай
			$host = $kernel->pub_str_prepare_set($host);
			$host = strip_tags($host);

			$shost = explode('.', $host);

			//убираем первый уровень
			unset($shost[0]);

			//подставляем вместо него звездочку
			$shost = '*.' . join('.', $shost);


			$ip = $kernel->pub_user_ip_get();


			$no_banned = $kernel->db_get_list_simple('_nobanned', "host  = '" . $host . "' OR host = '" . $shost . "' OR ip = " . ip2long($ip));

			if(!empty($no_banned))
			{
				foreach ($no_banned as $item)
				{

					//смотрим по ip адресу
					$itemip = long2ip($item['ip']);

					$item_ip[$itemip] = $item;

					//смотрим по хосту
					$item_host[$item['host']] = $item;

					//убираем первое вхождение - имя бота
					$explode_host = explode('.', $host);
					unset($explode_host[0]);
					$explode_host = '*.' . join('.', $explode_host);


					$item_shost[$explode_host] = $item;
				}

				//теперь вычисляем поточнее, чтобы знать какое правило приоритетнее

				//самый приоритетный - точное вхождение ip адреса
				if(!empty($item_ip[$ip]))
				{

					//переписываем наши правила для того кто есть в исключениях

					$is_block = $item_ip[$itemip]['block'];
					$is_notice = $item_ip[$itemip]['notice'];
				}
				//менее приоритетный - точное указание хоста
				elseif(!empty($item_host[$host]))
				{

					//переписываем наши правила для того кто есть в исключениях

					$is_block = $item_host[$host]['block'];
					$is_notice = $item_host[$host]['notice'];
				}
				//остался только подхост
				else
				{

					$is_block = $item_shost[$explode_host]['block'];
					$is_notice = $item_shost[$explode_host]['notice'];

				}
				//если попало в список исключений, то банить естественно не надо
				$is_ban = '0';
			}
			else
			{

				//за тор - только лог
				if($attack_type = 'tor')
				{
					$is_block = '0';
					$is_ban = '0';
					$is_notice = '0';
				}
				else
				{
					$is_block = $this->config['block_options']['block_matrix'][$type][$attacks[$attack_type]];
					$is_ban = $this->config['ban_options']['ban_matrix'][$type][$attacks[$attack_type]];
					$is_notice = $this->config['notice_options']['notice_matrix'][$type][$attacks[$attack_type]];
				}

				$nb_config['is_ban'] = $is_ban;
				$nb_config['is_block'] = $is_block;
				$nb_config['is_notice'] = $is_notice;
			}
		}
		else
		{
				$is_block 	= $nb_config['is_block'];
				$is_ban 	= $nb_config['is_ban'];
				$is_notice 	= $nb_config['is_notice'];
		}


		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			//забираем последний ip адрес
			$fip = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
			$fip = end($fip);

			//проверка, чтобы не подсунули ерунду какую-нибудь
			if (filter_var($fip, FILTER_VALIDATE_IP))
			{
				$forward_ip = $fip;
			}
			else
			{
				$forward_ip = '';
			}
		}

		$this->template = $kernel->pub_template_parse('admin/templates/default/security_notice.html');

		$source[]   = '%site_url%';
		$replace[]  = $kernel->pub_str_prepare_set($kernel->pub_http_host_get());

		$source[]   = '%type_attack%';
		$replace[]  = $kernel->pub_str_prepare_set($attack_type);


		$source[]   = '%method%';
		$replace[]  = $kernel->pub_str_prepare_set($type);


		$source[]   = '%user_agent%';
		$replace[]  = $kernel->pub_str_prepare_set(htmlentities($_SERVER['HTTP_USER_AGENT']));

		$source[]   = '%http_adress%';
		$replace[]  =  $kernel->pub_str_prepare_set(htmlentities($_SERVER['REQUEST_URI']));

		$source[]   = '%ip_adress%';
		$replace[]  = $kernel->pub_user_ip_get();

		$source[]   = '%date%';
		$replace[]  = date('Y-m-d G:i:s');

		$source[]   = '%code%';
		$replace[]  = $kernel->pub_str_prepare_set(htmlentities($value));

		//ip адрес за прокси
		if (!empty($forward_ip))
		{
			$source[] = '%forward_ip%';
			$replace[] = str_replace($this->template['forward_ip'], '%forward_ip%', $forward_ip);
		}
		else
		{
			$source[] = '%forward_ip%';
			$replace[] = 'Не выявлено';
		}

		//использование других ip адресов
		if (!empty($_SESSION['user_ips'][1]))
		{
			$source[] = '%another_ips%';
			$replace[] = str_replace($this->template['another_ips'], '%another_ips%', explode(',', $_SESSION['user_ips']));
		}
		else
		{
			$source[] = '%another_ips%';
			$replace[] = 'Не выявлено';
		}
		//использование других ip адресов
		if (!empty($_SESSION['user_ips'][1]))
		{
			$source[] = '%another_ips%';
			$replace[] = str_replace($this->template['another_ips'], '%another_ips%', explode(',', $_SESSION['user_ips']));
		}
		else
		{
			$source[] = '%another_ips%';
			$replace[] = 'Не выявлено';

		}




		//записываем все это дело в лог!
		$message = $this->template['log'];
		$message = str_replace($source, $replace, $message);

		$this->log($message);

		//извещение админа
		if ($is_notice == '1')
		{

			//извещение на почту
			if ($this->config['notice_options']['notice_mail'] == 1)
			{
				$message = $this->template['mail'];
				$message = str_replace($source, $replace, $message);

				$this->mail($message);
			}

			//извещение sms
			if ($this->config['notice_options']['notice_sms'] == 1)
			{

				$message = $this->template['sms'];
				$message = str_replace($source, $replace, $message);

				include $kernel->pub_site_root_get() . '/components/sms/sms.php';

				$sms = new Z_Service_Sms($this->config['other_options']['other_sms_login']);

				$sms->send($this->config['other_options']['other_phone'],  $message);

			}

			//извещание push
			if ($this->config['notice_options']['notice_push'] == 1)
			{
				$message = $this->template['push'];
				$message = str_replace($source, $replace, $message);

				$this->send_push($message);
			}

		}

		//бан пользователя
		if ($is_ban == '1')
		{
			$message = $this->template['ban'];
			$message = str_replace($source, $replace, $message);
			$this->set_ban($message);
		}

		//блокировка
		if ($is_block == '1')
		{
			$message = $this->template['block'];
			$this->set_block($message);
		}
	}


	/**
	 * Фильтрация данных
	 */
	public function input_filter()
	{

		$methods = array('GET', 'POST', 'COOKIE', 'SERVER', 'REQUEST');

		foreach($methods as $method)
		{

			//сначала забираем всю инфу из суперглобальных переменных
			switch($method)
			{
				case 'GET':     $row = $_GET;       break;
				case 'POST':    $row = $_POST;      break;
				case 'COOKIE':  $row = $_COOKIE;    break;
				case 'SERVER':  $row = $_SERVER;    break;
				case 'REQUEST': $row = $_REQUEST;   break;
				default: $row = array(); break;
			}

			if (!empty($row))
			{
				//бага в php, не на каждой конфигурации все хорошо с переменными _SERVER и _ENV
				if ($method == 'SERVER')
				{
					$row = filter_var_array($_SERVER, FILTER_DEFAULT);
				}
				//также фильтруем REQUEST
				elseif($method == 'REQUEST')
				{

					$row = filter_var_array($_REQUEST, FILTER_DEFAULT);
				}
				else
				{
					$row =  filter_input_array(constant('INPUT_'.$method), FILTER_DEFAULT);
				}

				$row = $this->input_filter_array($row);
			}




			//после фильтрации, перезаписываем массив суперглобальных переменных
			switch($method)
			{
				case 'GET':     $_GET = $row; break;
				case 'POST':    $_POST = $row; break;
				case 'COOKIE':  $_COOKIE  = $row; break;
				case 'SERVER':  $_SERVER  = $row; break;
				case 'REQUEST':  $_REQUEST = $row; break;
			}

		}
	}


	/**
	 * Фильтрация глобальных переменных
	 * @param $row
	 * @return mixed
	 */
	public function input_filter_array($row)
	{
		foreach($row as $key=>$item)
		{
			if (!is_array($item))
			{
				// убираем все атрибуты в тегах
				$item = preg_replace('#(</?\\w+)(?:\\s(?:[^<>/]|/[^<>])*)?(/?>)#ui', '$1$2', $item);

				//разрешаем только определенные теги
				$item = strip_tags($item, $this->config['tags_filter']);

				//убираем значения переменных и языковые переменные тоже
				$item = preg_replace(Array('#%(.*)%#', '~\[(.*)\]~', '~\[~', '~\]~', '/\[@function-/'), '', $item);

				//убираем кавычки '
				$row[$key] = str_replace("'", '&#039;', $item);

			}
			else
			{
				$row[$key] = $this->input_filter_array($item);
			}
		}

		return $row;

	}


	/**
	 * Проверка, заблокирован ли пользователь или нет
	 * @return int 1 - заблокирован, 0 - нет
	 */
	public function is_banned()
	{
		global $kernel;

		$ip = $kernel->pub_user_ip_get();

		$mask_ip = explode('.', $ip);
		$mask_ip = $mask_ip[0].'.'.$mask_ip[1].'.'.$mask_ip[2].'.0';

		$ips[] = ip2long($kernel->pub_user_ip_get());
		$ips[] = ip2long($mask_ip);

		if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			//проверим, чтобы не подсунули ерунду
			if (filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
			{
				$ips[] =  ip2long($_SERVER['HTTP_X_FORWARDED_FOR']);
			}
		}

		//также проверим ip адреса, которые присутвовали в сессии
		if (!empty($_SESSION['user_ips'][1]))
		{
			foreach($_SESSION['user_ips'] as $ip_a)
			{
				$ips[] =  ip2long($ip_a);
			}
		}

		$ips = join(',', $ips);

		$bans = $kernel->db_get_list_simple('_banned', 'ip IN ('.$ips.') AND date_unbanned > NOW()');

		if (!empty($bans))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}


	/**
	 * Проверка нету ли хоста или ip адреса в списке исключений
	 * @return int
	 */
	public function is_nobanned()
	{

		global $kernel;

		//проверяем хост
		$host = gethostbyaddr($_SERVER['REMOTE_ADDR']);

		// с введением многосимвольных доменов, может быть что-то не предвиденное, поэтому вырезаем все на всякий случай
		$host = $kernel->pub_str_prepare_set($host);
		$host = strip_tags($host);

		$shost = explode('.', $host);

		//убираем первый уровень
		unset($shost[0]);

		//подставляем вместо него звездочку
		$shost = '*.' . join('.', $shost);


		$ip = $kernel->pub_user_ip_get();


		$no_banned = $kernel->db_get_list_simple('_nobanned', "host  = '" . $host . "' OR host = '" . $shost . "' OR ip = " . ip2long($ip));

		if (!empty($no_banned))
		{
			return 1;
		}
		else
		{
			return 0;
		}

	}


	/**
	 * Бан пользователя
	 * @param string $message   - сообщение админу при бане (необзательное)
	 * @param string $ip        - ip адрес бана (необзательное)
	 * @param string $agent     - userAgent (необзательное). В будующем предпологается также сделать бан по юзерагенту
	 */
	public  function set_ban($message='', $ip='', $agent='')
	{
		global $kernel;

		if (empty($ip))
		{
			$ip = $kernel->pub_user_ip_get();

			//если указаны звездочки, преобразовываем в 0
			$ip = str_replace('*', '0', $ip);
		}

		if (empty($message))
		{
			$message = 'Ручной бан';
		}

		//бан по маске
		if ($this->config['ban_options']['ban_subnet'] == 1)
		{
			$mask_ip = $this->mask_ip($ip);
			$ips[] = ip2long($mask_ip);
		}
		else
		{
			$ips[] = ip2long($kernel->pub_user_ip_get());
		}

		//если нужен бан forward_ip
		if($this->config['ban_options']['ban_forwarded_for'] == 1)
		{
			$mask_ip = NULL;
			if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			{
				//проверим, чтобы не подсунули ерунду
				if (filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
				{
					$forward_ip =  ip2long($_SERVER['HTTP_X_FORWARDED_FOR']);
					//включен бан по маске
					if ($this->config['ban_options']['ban_subnet'] == 1)
					{
						$mask_ip = $this->mask_ip($forward_ip);
						$ips[] = ip2long($mask_ip);
					}
					else
					{
						$ips[] = ip2long($forward_ip);
					}
				}
			}
		}

		//также проверим ip адреса, которые присутвовали в сессии
		if (!empty($_SESSION['user_ips'][1]))
		{
			foreach($_SESSION['user_ips'] as $ip_a)
			{
				if ($this->config['ban_options']['ban_subnet'] == 1)
				{
					$mask_ip = $this->mask_ip($ip_a);
					$ips[] = ip2long($mask_ip);
				}
				else
				{
					$ips[] = ip2long($ip_a);
				}
			}
		}

		$ips = array_unique($ips);

		$rec['note']        = $message;
		$rec['user_agent']  = $kernel->pub_str_prepare_set(htmlentities($_SERVER['HTTP_USER_AGENT']));
		$rec['date_banned'] = date('Y-m-d H:i:s');

		$rec['date_unbanned'] = date('Y-m-d H:i:s', strtotime('+1 hours'));

		foreach($ips as $ip_adress)
		{
			$rec['ip'] = $ip_adress;
			$kernel->db_add_record('_banned', $rec, 'REPLACE');
		}
		//блокируем дальнейшую работу скрипта
		$this->set_block('Доступ к сайту запрещен!');
	}


	/**
	 * Логирование запросов
	 * @param $message - сообщение, которое надо залогировать
	 */
	public function log($message)
	{
		global $kernel;

		$attack_type = $this->break->attack_type;

		$dir = $kernel->pub_site_root_get().'/logs';

		if (is_writable($dir) && is_dir($dir))
		{
			$filename = $dir.'/'.$attack_type.'_log.txt';
			$f = @fopen($filename, 'a');
			@fwrite($f, $message);
			@fclose($f);
		}
	}


	/**
	 * Отправка почтовых сообщений с информацией об атаке на сайт
	 * @param $message
	 */
	public function mail($message)
	{
		global $kernel;
		$tmp_file = tempnam(sys_get_temp_dir(), 'Qar');

		$value = $this->break->value;
		$f = @fopen($tmp_file, 'w');
		@fwrite($f, 'Нежелательное содержание: ' . htmlentities($value));
		@fclose($f);

		$mails = explode(',', $this->config['other_options']['other_mail']);
		$attach_files = Array($tmp_file);
		$kernel->pub_mail($mails, 'Admin', $this->config['other_options']['other_mail'], $kernel->pub_http_host_get().' Admin', 'Попытка аттаки на сайт!', $message, 1, "", $attach_files);

		@unlink($tmp_file);
	}


	/**
	 * Отправка push сообщения
	 * @param $message
	 */
	public  function send_push($message)
	{
		global $kernel;
		include $kernel->pub_site_root_get() . '/components/push/push.php';

		//поменять потом токен
		$push = new Pushover('aQcPEWEm6NRtoWpCdjaTFA6NzA5zHU');
		$push->userToken = $this->config['other_options']['other_push_login'];
		$push->notificationMessage = $message;
	}


	/**
	 * Блокировка действий пользователя
	 * @param $message
	 */
	public function set_block($message)
	{
		//можно еще поставить куку, которая на 2-3 минуты блокирует пользователя, или сделать 2-3 минутный бан, но пока только блокировка
		die($message);
	}


	/**
	 * Декодирование url строки
	 * @param $type
	 * @param $key
	 * @param $value
	 * @param $attack_type
	 * @return string
	 */
	public static function urldecode($type, $key, $value, $attack_type)
	{

		return urldecode($value);

	}


	/**
	 * Маска ip адреса
	 * @param $ip
	 * @return string
	 */
	public  function mask_ip($ip)
	{
		$mask_ip = NULL;
		$mask_ip = explode('.', $ip);
		$mask_ip = $mask_ip[0].'.'.$mask_ip[1].'.'.$mask_ip[2].'.0';

		return $mask_ip;
	}

}