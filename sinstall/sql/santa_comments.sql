DROP TABLE IF EXISTS `%PREFIX%_comments`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_comments` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `module_id` varchar(255) NOT NULL,
  `page_id` varchar(255) default NULL,
  `page_sub_id` varchar(255) default NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `available` tinyint(1) unsigned default '1',
  `txt` text NOT NULL,
  `author` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `module_id` (`module_id`),
  KEY `date` (`date`),
  KEY `time` (`time`),
  KEY `available` (`available`),
  KEY `author` (`author`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_comments` VALUES('1', 'comments1', 'news', 'id=6,', '2014-11-04', '14:19:15', 1, 'Классная новость', 'Посетитель');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_comments` VALUES('2', 'comments1', 'news', 'id=6,', '2014-11-04', '15:19:37', 1, 'Комментарий к новости', 'Админ');